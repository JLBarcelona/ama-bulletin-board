/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : cleaning

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-01-20 17:34:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for collection
-- ----------------------------
DROP TABLE IF EXISTS `collection`;
CREATE TABLE `collection` (
  `collectionId` int(255) NOT NULL AUTO_INCREMENT,
  `staffId` int(255) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `remarks` longtext,
  `bag_number` int(11) DEFAULT NULL,
  `is_finish` datetime DEFAULT NULL,
  `is_delivered` datetime DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`collectionId`),
  KEY `customer` (`customerId`),
  KEY `staff` (`staffId`),
  CONSTRAINT `customer` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON UPDATE CASCADE,
  CONSTRAINT `staff` FOREIGN KEY (`staffId`) REFERENCES `staff` (`staffId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of collection
-- ----------------------------
INSERT INTO `collection` VALUES ('105', '3', '27', 'Hello', null, null, null, '2020-01-20 11:12:16', null);

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customerId` int(255) NOT NULL AUTO_INCREMENT,
  `staffId` int(255) DEFAULT NULL,
  `customerName` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `customerArea` varchar(255) DEFAULT NULL,
  `date_register` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`customerId`),
  KEY `staffs` (`staffId`),
  CONSTRAINT `staffs` FOREIGN KEY (`staffId`) REFERENCES `staff` (`staffId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('22', '3', 'Customer A', 'Mount Fuji', null, null);
INSERT INTO `customer` VALUES ('23', '3', 'Customer B', 'Tokyo', '2020-01-08 11:09:44', null);
INSERT INTO `customer` VALUES ('24', '3', 'graham', 'graham', '2020-01-08 11:12:24', null);
INSERT INTO `customer` VALUES ('26', '3', '預り品登録', '?????', '2020-01-08 11:13:44', null);
INSERT INTO `customer` VALUES ('27', '3', 'ジョンルイスバルセロナ', 'Tokyo', '2020-01-08 12:37:04', null);

-- ----------------------------
-- Table structure for damage
-- ----------------------------
DROP TABLE IF EXISTS `damage`;
CREATE TABLE `damage` (
  `damageId` int(255) NOT NULL AUTO_INCREMENT,
  `collectionId` int(255) DEFAULT NULL,
  `is_checked` int(11) DEFAULT '0',
  `itemNo` int(255) DEFAULT NULL,
  `damage_type` varchar(255) DEFAULT NULL,
  `is_solved` int(1) DEFAULT '0',
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`damageId`),
  KEY `collection` (`collectionId`),
  CONSTRAINT `collection` FOREIGN KEY (`collectionId`) REFERENCES `collection` (`collectionId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of damage
-- ----------------------------
INSERT INTO `damage` VALUES ('11', '105', '0', '1', 'Missing Buttons', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('12', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('13', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('14', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('15', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('16', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('17', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('18', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('19', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);
INSERT INTO `damage` VALUES ('20', '105', '0', null, '', '0', '2020-01-20 11:12:18', null);

-- ----------------------------
-- Table structure for factory
-- ----------------------------
DROP TABLE IF EXISTS `factory`;
CREATE TABLE `factory` (
  `factoryId` int(255) NOT NULL AUTO_INCREMENT,
  `factoryName` varchar(255) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`factoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of factory
-- ----------------------------
INSERT INTO `factory` VALUES ('1', 'Factory A', null, null);
INSERT INTO `factory` VALUES ('2', 'Factory B', null, null);
INSERT INTO `factory` VALUES ('3', 'Factory C', null, null);
INSERT INTO `factory` VALUES ('4', 'Factory D', null, null);
INSERT INTO `factory` VALUES ('5', 'Factory E', null, null);
INSERT INTO `factory` VALUES ('6', 'Factory F', null, null);

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `itemId` int(255) NOT NULL AUTO_INCREMENT,
  `collectionId` int(255) DEFAULT NULL,
  `factoryId` int(255) DEFAULT NULL,
  `itemName` varchar(255) DEFAULT NULL,
  `quantity` int(255) DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`itemId`),
  KEY `collections` (`collectionId`),
  KEY `factory` (`factoryId`),
  CONSTRAINT `collections` FOREIGN KEY (`collectionId`) REFERENCES `collection` (`collectionId`) ON UPDATE CASCADE,
  CONSTRAINT `factory` FOREIGN KEY (`factoryId`) REFERENCES `factory` (`factoryId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('11', '105', '1', 'Y-shirt white', '12', '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('12', '105', '1', '', null, '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('13', '105', '1', '', null, '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('14', '105', '1', '', null, '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('15', '105', '1', '', null, '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('16', '105', '1', '', null, '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('17', '105', '1', '', null, '2020-01-20 11:12:17', null);
INSERT INTO `items` VALUES ('18', '105', '1', '', null, '2020-01-20 11:12:18', null);
INSERT INTO `items` VALUES ('19', '105', '1', '', null, '2020-01-20 11:12:18', null);
INSERT INTO `items` VALUES ('20', '105', '1', '', null, '2020-01-20 11:12:18', null);

-- ----------------------------
-- Table structure for item_photo
-- ----------------------------
DROP TABLE IF EXISTS `item_photo`;
CREATE TABLE `item_photo` (
  `photoId` int(255) NOT NULL AUTO_INCREMENT,
  `itemId` int(255) DEFAULT NULL,
  `filename` longtext,
  `file_path` longtext,
  `date_entry` datetime DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  PRIMARY KEY (`photoId`),
  KEY `items` (`itemId`),
  CONSTRAINT `items` FOREIGN KEY (`itemId`) REFERENCES `items` (`itemId`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item_photo
-- ----------------------------

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `staffId` int(255) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT NULL,
  `is_delete` datetime DEFAULT NULL,
  `date_entry` datetime DEFAULT NULL,
  PRIMARY KEY (`staffId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('3', 'Testing', 'Account', 'MALE', 'johnluisb14@gmail.com', 'bnj', '$2y$10$s9wN/AlXN1KaE.2uPeI3Je/C1K9mWbvcHZ6H4j1gKRIyeMWsUZC.K', null, null, null);
