-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2020 at 08:47 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ama_attandance`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attendance`
--

CREATE TABLE `tbl_attendance` (
  `attendance_id` int(255) NOT NULL,
  `class_id` int(255) DEFAULT NULL,
  `user_id` int(255) DEFAULT NULL,
  `date_attend` date DEFAULT NULL,
  `time_in` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `time_out` timestamp NULL DEFAULT NULL,
  `is_delete` int(1) DEFAULT '0',
  `ampm` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_attendance`
--

INSERT INTO `tbl_attendance` (`attendance_id`, `class_id`, `user_id`, `date_attend`, `time_in`, `time_out`, `is_delete`, `ampm`) VALUES
(3, 2, 30, '2019-10-03', '2019-10-03 04:57:07', '2019-10-03 04:57:00', 0, 'AM/PM'),
(4, 4, 30, '2019-10-03', '2019-10-03 04:57:28', '2019-10-03 04:57:00', 0, 'AM/PM'),
(5, 2, 30, '2020-02-24', '2020-02-24 04:44:14', '2020-02-24 04:44:00', 0, 'AM/PM');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `class_id` int(255) NOT NULL,
  `teacher_id` int(255) DEFAULT NULL,
  `subject` longtext,
  `course` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  `active` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_id`, `teacher_id`, `subject`, `course`, `year`, `section`, `active`) VALUES
(2, 27, 'Math', 'BSIT', '3', 'O1', 1),
(3, 29, 'Database Programming', 'BSCS', '2', 'P3', 1),
(4, 27, 'Database Programming', 'BSIT', '3', 'O1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(255) NOT NULL,
  `qrcode_id` int(255) DEFAULT NULL,
  `qrcode_path` longtext,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `course` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  `cp_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` longtext CHARACTER SET utf8,
  `avatar` longtext,
  `user_type` int(1) DEFAULT '2',
  `is_delete` int(1) DEFAULT '0',
  `verfication_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `qrcode_id`, `qrcode_path`, `fn`, `mn`, `ln`, `gender`, `address`, `course`, `year`, `section`, `cp_number`, `email`, `username`, `password`, `avatar`, `user_type`, `is_delete`, `verfication_code`) VALUES
(1, NULL, NULL, 'JL', 'Soriano', 'Barcelona', 'Male', 'Sagana Santiago City', '', '', '', '', 'sample@sample.com', 'admins', '$2y$10$urFKtW8AyFjc7WLjGkIppOrG0Tn/hUIS2BhzoUoN6XHKhCLBXaY7G', NULL, 1, 0, NULL),
(27, NULL, '', 'Hermo', 'S.', 'Imbag', 'Male', 'asdad', '', '', '', '0909209301239', 'asdadsa@asdasd.com', 'teacher', '$2y$10$urFKtW8AyFjc7WLjGkIppOrG0Tn/hUIS2BhzoUoN6XHKhCLBXaY7G', '', 2, 0, NULL),
(29, NULL, NULL, 'Nico', 'S.', 'Nico', 'Male', 'Sagana Santiago City', NULL, NULL, NULL, '0909209301239', 'johnluis@asda.com', 'JohnLuisB829', '$2y$10$eqbxYc4..fTYYO1GEmSGq.nce/eXbrVJcTMlHBVFOlaGmeMCRtNfG', NULL, 2, 0, NULL),
(30, 32741, '5dd5791c54c4f.png', 'Greyson Cody', 'Almojera', 'Barcelona', 'Male', 'Plaridel Santiago City', 'BSIT', '3', 'O1', '09754952519', 'johnluisb14@gmail.com', 'gc', '$2y$10$HW.dqAIR6fQDR4XSaNNmZO6tkA1uQjNbqtj7ZtyNQvMxF3rx0zTiG', NULL, 3, 0, 'RUCcx30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attendance`
--
ALTER TABLE `tbl_attendance`
  MODIFY `attendance_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `class_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
