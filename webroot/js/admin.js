var url = '../function/function.php';
var on_edit = 0;
var on_profile = 0;


 function show_view(why,where,when,extention,filename,posted_by,posted_on,title){
    // alert()
    $("#title_s").text(title);
    $("#why_s").text(why);
    $("#where_s").text(where);
    $("#when_s").text(when);
    $("#extention").text(extention);
    $("#filename").text(filename);
    $("#posted_by").text(posted_by);
    $("#posted_on").text(posted_on);
    $("#modal_show").modal('show');
  }

  function show_all_post(){
    $.ajax({
      url:url,
      type:"POST",
      data:'action=show_all_post_new' + '&type=' + $("#filter_post").val(),
      cache:false,
      success:function(data){
        $("#post_data").html(data);
        $("#tbl_all_post_new").dataTable();
        }
    });
  }

  function post_types(val){
    if (val == "Announcement") {
      clear_upload();
      // $("#is_file").hide('fast');
      $("#btn_delete").click();
    }else if (val == "Activity") {
      clear_upload();
      // $("#is_file").hide('fast');
      $("#btn_delete").click();
    }else if (val == "Schedule") {
      // $("#is_file").show('fast');
    }
  }


  function clear_upload(){
    $("#file").val('');
    $("#file_name").val('');
    $("#btn_upload").attr('disabled', false);
    $("#img_preview").html('<input type="hidden" name="fie_path" id="file_path" value="">');
  }




  function delete_files(file_path){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=delete_file' + '&file_path=' + file_path,
      cache:false,
      success:function(data){
        // alert(data);
        if (data.trim() == 1) {
          $("#previews").addClass('animated zoomOutDown');
         setTimeout(function(){
          clear_upload();
         },1200);
          $("#previews").hide('fast');
        }
      }
    });
  }


  $("#file").on('change', function(e){
    e.preventDefault();
        var fullPath = this.value;
        if (fullPath != "" || fullPath != null) {
          $("#up_btn").attr('disabled', false);
          var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }

            var form_data = new FormData();

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("file").files[0]);
  
            form_data.append("file", document.getElementById('file').files[0]);
            //get file extension 
            var f = document.getElementById("file").files[0];
            var fsize = f.size||f.fileSize;

            var extensions = filename.split('.').pop();
            
            if(fsize > 2000000){
               swal("Oops!","File size is too large!","error");
               clear_upload();
               $("#img_preview").html('<input type="hidden" name="fie_path" id="file_path" value="">');
            }
            else if(fsize <= 2000000){
              $("#file_name").val(filename);
              $.ajax({
                   url:"upload.php",
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                  beforeSend:function(){
                    $("#btn_upload").attr('disabled', true);
                  },
                  success:function(data){
                    if (data.trim() != 0) {
                      $("#img_preview").html(data);
                    }
                  }
                });
               // return true;
            }
            else{
              swal("Oops!","Invalid file format !","error");
              return false;
            }


        }else{
          // $("#up_btn").attr('disabled', true);
        }
    });


 // $("#file").on('change', function(e){
 //    e.preventDefault();
 //        var fullPath = this.value;
 //        if (fullPath != "" || fullPath != null) {
 //          $("#up_btn").attr('disabled', false);
 //          var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
 //            var filename = fullPath.substring(startIndex);
 //            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
 //                filename = filename.substring(1);
 //            }

 //            var form_data = new FormData();

 //            var oFReader = new FileReader();
 //            oFReader.readAsDataURL(document.getElementById("file").files[0]);
  
 //            form_data.append("file", document.getElementById('file').files[0]);
 //            //get file extension 
 //            var f = document.getElementById("file").files[0];
 //            var fsize = f.size||f.fileSize;

 //            var extensions = filename.split('.').pop();
            
 //            if(fsize > 2000000){
 //               swal("Oops!","File size is too large!","error");
 //               clear_upload();
 //               $("#img_preview").html('<input type="hidden" name="fie_path" id="file_path" value="">');
 //            }
 //            else if(extensions == 'png' || extensions == 'jpg' || extensions == 'jpeg' || extensions == 'PNG' || extensions == 'JPG' || extensions == 'JPEG'){
 //              $("#file_name").val(filename);
 //              $.ajax({
 //                   url:"upload.php",
 //                    method:"POST",
 //                    data: form_data,
 //                    contentType: false,
 //                    cache: false,
 //                    processData: false,
 //                  beforeSend:function(){
 //                    $("#btn_upload").attr('disabled', true);
 //                  },
 //                  success:function(data){
 //                    if (data.trim() != 0) {
 //                      $("#img_preview").html(data);
 //                    }
 //                  }
 //                });
 //               // return true;
 //            }
 //            else{
 //              swal("Oops!","Invalid file format !","error");
 //              return false;
 //            }


 //        }else{
 //          // $("#up_btn").attr('disabled', true);
 //        }
 //    });


  function show_wall(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_all_post',
      cache:false,
      beforeSend:function(){
        $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
      },
      success:function(data){
        // alert(data);
         $("#loader").html('');
         $("#wall_data").html(data);
      }
    });
  }

  function show_wall_deleted(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_all_post_deleted',
      cache:false,
      beforeSend:function(){
        $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
      },
      success:function(data){
        // alert(data);
         $("#loader").html('');
         $("#wall_data").html(data);
      }
    });
  }


  function show_others_wall(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_post_other',
      cache:false,
      beforeSend:function(){
        // $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
      },
      success:function(data){
        // alert(data);
      
         $("#other_wall_data").html(data);
      }
    });
  }

  // function post_wall(){
  //   var type = $("#type");
  //   var title = $("#title");
  //   var content = $("#content");
  //   var file_path = $("#file_path");
  //   var file = $("#file");
  //   var post_id = $("#post_id");
  //   var exp_date = $("#exp_date");

  //   if (type.val() == "" || type.val() == null) {
  //     type.focus();
  //   }
  //   else if (title.val() == "") {
  //     title.focus();
  //   }
  //   else if (content.val() == "") {
  //     content.focus();
  //   }else if (exp_date.val() == "" || exp_date.val() == null) {
  //     exp_date.focus();
  //   }
  //   else if (type.val() == "Schedule" && file.val() == "" ) {
  //     file_path.focus();
  //     swal("Error","Please upload photo!","error");
  //   }else{
  //     var mydata = 'action=save_announcement' + '&type=' + type.val() + '&title=' + title.val() + '&content=' + content.val() + '&file_path=' + file_path.val() + '&post_id=' + post_id.val() + '&exp_date=' + exp_date.val();
      
  //     $.ajax({
  //       type:"POST",
  //       url:url,
  //       data:mydata,
  //       cache:false,
  //       beforeSend:function(){
  //         $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
  //       },
  //       success:function(data){
        
  //         if (data.trim() == 1) {
  //           // swal("Success","Announcement has been posted!","success");
  //           clear_upload();
  //           if (type.val() == "Schedule") {
  //             $("#is_file").hide('fast');
  //             $("#btn_delete").click();
  //           }
  //           type.val(null);
  //           title.val('');
  //           content.val('');
  //           file_path.val('');
  //           exp_date.val('');
  //           $("#loader").html('');
  //           post_id.val('');
  //           show_wall();
  //           $("#btn_cancel").hide('fast');

  //         }else{
  //           alert(data);
  //         }
  //       }
  //     });

  //   }


  // }
 

 function edit_image(path,fullpath){
    // $("#is_file").show('fast');
    if (path != "") {
       var extensions = path.split('.').pop();
       var data = '';
      if (extensions == 'png' || extensions == 'jpg' || extensions == 'jpeg' || extensions == 'PNG' || extensions == 'JPG' || extensions == 'JPEG') {
          data += '<div class="animated jackInTheBox" id="previews">';
          data += '<input type="hidden" name="file_path" id="file_path" value="'+path+'">';
          data += '<img src="'+fullpath+'" class="img-fluid rounded  " width="110">';
          data += '<button class="btn btn-danger btn-block mt-1" id="btn_delete" onclick="delete_files(\''+fullpath+'\');">Delete</button>';
          data += ' </div>';
          $("#img_preview").append(data);
      }else{
          data += '<div class="animated jackInTheBox" id="previews">';
          data += '<input type="hidden" name="file_path" id="file_path" value="'+path+'">';
          data += '<img src="../webroot/img/files.png" class="img-fluid rounded  " width="110">';
          data += '<div class="text-sm">'+path+'</div>';
          data += '<button class="btn btn-danger btn-block mt-1" id="btn_delete" onclick="delete_files(\''+fullpath+'\');">Delete</button>';
          data += ' </div>';
          $("#img_preview").append(data);
      }
    }else{
      
    }

   

    // console.log(extensions);
        
  }


  function edit_post(id,title,content,type,path,fullpath,exp_date){
    // console.log(path);
      if (path != "" || path != null) {
        edit_image(path,fullpath);
        $("#file_name").val(path);
        $("#file_path").val(path);
      }

      
    $("#post_id").val(id);
    $("#type").val(type);
    $("#title").val(title);
    $("#content").val(content);
    $("#exp_date").val(exp_date);
     fetch_content();
    $("#is_file").hide('fast');
    $("#btn_cancel").show('fast');
   
  }


  function cancel_edit_post(){
    var type = $("#type");
    var title = $("#title");
    var content = $("#content");
    var file_path = $("#file_path");
    var file = $("#file");
    var post_id = $("#post_id");
    $("#exp_date").val('');

    $("#btn_cancel").hide('fast');

     clear_upload();
    // if (type.val() == "Schedule") {
    //   $("#is_file").hide('fast');
    //   $("#btn_delete").click();
    // }
    type.val('Announcement');
    title.val('');
    content.val('');
    file_path.val('');
    $("#loader").html('');
    post_id.val('');
    show_wall();

    $("#modal_posting").modal('hide');

  }


 

  function delete_post(id){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete this post ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_post' + '&id=' + id,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            show_wall(); show_others_wall();
             swal("Success","Post has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}


function delete_post_from_history(id){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete this post from backup ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_post_backup' + '&id=' + id,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
           show_wall_deleted();
             swal("Success","Post has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}


function set_show_others_wall_update(){
  setInterval(function(){
    show_others_wall();
  },9000);
}


  function show_accounts(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_accounts',
      cache:false,
      success:function(data){
       $("#account_data").html(data);
       $("#tbl_accounts").DataTable();
      }
    }); 
  }

  function onedit(){
    $("#add_account").modal('show');
    on_edit = 1;
  }


  function edit(profid,fn,mn,ln,gender,email,password,department,position){
    $("#profid").val(profid);
    $("#fn").val(fn);
    $("#mn").val(mn);
    $("#ln").val(ln);
    $("#gender").val(gender);
    $("#email").val(email);
    $("#password").val(password);
    $("#department").val(department);
    $("#position").val(position);
  }

  function alphanumeric() { 
    var txt = document.getElementById("password");
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if(txt.value.match(passw)) { 
      // alert('Correct, try another...')
      return true;
    }
    else{ 
      // alert('Wrong...!')
      return false;
    }
  } 


   function save_account(){
    var profid = $("#profid");
    var fn = $("#fn");
    var mn = $("#mn");
    var ln = $("#ln");
    var gender = $("#gender");
    var department = $("#department");
    var position = $("#position");
    var email = $("#email");
    var password = $("#password");
    var sec_question = $("#sec_question");
    var sec_answer = $("#sec_answer");

 


    if (fn.val() == "" || fn.val() == null) {
      fn.focus();
    }
    else if (mn.val() == "" || mn.val() == null) {
      mn.focus();
    }
    else if (ln.val() == "" || ln.val() == null) {
      ln.focus();
    }
    else if (gender.val() == "" || gender.val() == null) {
      gender.focus();
    }
    else if (department.val() == "" && on_edit == 0 || department.val() == null && on_edit == 0) {
      department.focus();
    }
    else if (position.val() == "" && on_edit == 0 || position.val() == null && on_edit == 0) {
      position.focus();
    }
    else if (email.val() == "" || email.val() == null) {
      email.focus();
    }
    else if (password.val() == "" && on_edit == 0) {
      password.focus();
    }else if (alphanumeric() == false && on_edit == 0) {
      swal("Oops!","Password Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters","error");
      password.focus();
    }else if (sec_question.val() == "" && on_edit == 0 || sec_question.val() == null && on_edit == 0) {
      sec_question.focus();
      swal("Oops!","Security Question is required!","error");
    }
    else if (sec_answer.val() == "" && on_edit == 0 || sec_answer.val() == null && on_edit == 0) {
      sec_answer.focus();
      swal("Oops!","Security Answer is required!","error");
    }else{
      var mydata = 'action=save_account_user'+ '&profid=' + profid.val() +'&fn=' + fn.val() +'&mn=' + mn.val() +'&ln=' + ln.val() +'&gender=' + gender.val() +'&email=' + email.val() +'&password=' + password.val() + '&department=' + department.val() + '&position=' + position.val() + '&sec_question=' + sec_question.val() + '&sec_answer=' + sec_answer.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          console.log(data);
          if (data.trim() == 202) {
            swal("Sorry","Username is already used!","error");
            email.focus();
          }else if (data.trim() == 1) {
            if (on_profile == 1) {
              swal("Success","Account has been saved!","success");
            }else{
                 edit('','','','','','','','');
                 sec_question.val('');
                 sec_answer.val('');
            }
            show_accounts();
            swal("Success","Account has been saved!","success");
            setTimeout(function(){
               location.reload();
            },200);
            if (on_edit == 1) {
                $("#add_account").modal('hide');
            }
            // alert();
          }else{
            console.log(data);
          }
        }
      });
    }


  }


  function delete_account(id, name, type){
    var message = '';

    if (type == 1) {
      message ='deactivate';
    }else if (type == 0) {
      message= 'acivate';
    }

  swal({
      title: "Are you sure?",
      text: "Do you want to "+message+" "+name+"?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_account' + '&id=' + id + '&type=' +type,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            show_accounts();
             swal("Success","Profile has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}



  