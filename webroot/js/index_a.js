status_page = 1;
    var timer = new easytimer.Timer();
    var hh = '00';
    var mm = '00';
    var ss = '60';


    function start_timer(){
      console.log()
      timer.start({countdown: true, startValues: {hours: Number(hh),minutes: Number(mm),seconds: Number(ss)}});
          $('#counter .values').html(timer.getTimeValues().toString());
          timer.addEventListener('secondsUpdated', function (e) {
              $('#counter .values').html(timer.getTimeValues().toString());
              var val = timer.getTimeValues().toString();
          });

          timer.addEventListener('targetAchieved', function (e) {
              // $('#counter .values').html('Time Out');
            
              if (status_page == 2) {
                 home_dash();
                 status_page = 1;
              }
          });
    }

    function change_page(){
      if (status_page == 2) {
        status_page = 1;
        timer.stop();
      }else{
        status_page = 2;
        start_timer();
      }
      
    }

  function flip_board(){
    show_all_marquee();
    console.log(status_page);
    // // console.log(status_page + ' status');
    // $("#btnicon").toggleClass('fa-bars fa-times');
    // $("#btnicon").toggleClass('flipInX flipInY');
    // $("#video_page").toggleClass('fadeOut fadeIn');
    $("#services").removeClass('fadeOut');
    $("#services").addClass('fadeIn');
    // $("#nav_id").toggleClass('opacity-on opacity-off');
  
    $("#video_page").hide('fast');
    $("#services").show('fast');
    $("#container").animate({'margin-top':'0px'});
    $("#br").hide('fast');
    $("#marquee").hide('fast');


    if (status_page == 1) {
       // $("#btn_flip").attr('onclick', "flip_board(); change_page();");
       // $("#btn_flip").attr('data-toggle', "");
       // $(".dropdown-menu").hide('fast');
       // $("#sidebar").hide('fast');
       // $("#contents").animate({'margin-left':'0px'});
       // $("#btn_flip").show('fast');
       // $("#container").animate({'margin-top':'10px'});
       // $("#br").hide('fast');
       // $("#marquee").hide('fast');
    }else{
       // $("#btn_flip").removeAttr('onclick', "flip_board(); change_page();");
       // $("#btn_flip").attr('data-toggle', "dropdown");
       // $(".dropdown-menu").show('fast');
       // $("#btn_flip").hide('fast');
       // $("#sidebar").show('fast');
       // $("#contents").animate({'margin-left':'15%'});
       // $("#container").animate({'margin-top':'0px'});
       // $("#br").show('fast');
       // $("#marquee").show('fast');
       // $("#br").animate({'display':'none'});
       

    }
  }


  function home_dash(){
     show_all_marquee();
     console.log(status_page);
      // console.log(status_page + ' status');
      $("#video_page").show('fast');
      $("#services").hide('fast');
     
       // $("#sidebar").show('fast');
       // $("#contents").animate({'margin-left':'15%'});
       $("#container").animate({'margin-top':'10px'});
       $("#br").show('fast');
       $("#marquee").show('fast');
  }


$(document).ready(function(){
  $(document).on('mousemove', function(event){
      ss = '10';
      timer.stop();
      setTimeout(function(){
        if (status_page == 2) {
          start_timer();
        }else{
          timer.stop();
        }
      },1000);
      // console.log(ss);
  });
});



var url = "function/function.php";

function show_all_marquee(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_announcement',
    cache:false,
    success:function(data){
      $("#announcement_panel").html(data);
    }
  });
}


function show_previews(title,arrays,file_path){
    $("#modal_preview_title").text(title);
    // $("#preview_panel").html();
    // alert(file_path);
    $("#preview_panel").html('<div  class="text-center p-4"><img src="webroot/img/load.gif" width="50"></div>');
    var data = '';
    var arr = arrays.split("~");

    for (var i = 0; i < arr.length; i++) {
      // arr[i];
      // console.log(arr[i]);
      var filename = arr[i];

       // get file extension 
      var extensions = filename.split('.').pop();
      
      var is_active = (i == 0)? 'active' : '';
      if(extensions == 'pdf' || extensions == 'png' || extensions == 'jpg' || extensions == 'jpeg' || extensions == 'PNG' || extensions == 'JPG' || extensions == 'JPEG' || extensions == 'GIF' || extensions == 'gif'){
         data += ' <div class="carousel-item '+is_active+'">';
           // data += '<img src="'+file_path+filename+'" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">';
          data += '<div class="text-center"><img src="webroot/img/img.png" class="  post_bg" style="width: 80%; height:80%; background-image: url('+file_path+filename+');"></div>';
           data += ' </div>';
      }else{
           data += ' <div class="carousel-item '+is_active+'">';
           // data += '<img src="'+file_path+filename+'" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">';
           data += '<div class="text-center"><img src="webroot/img/img.png" class="  post_bg" style="width: 80%; height:80%; background-image: url(webroot/img/files.png);"></div>';

           data += ' <div class="text-center"><a class="btn btn-block btn-success btn-block" href="'+file_path+filename+'">Download '+filename+'</a></div>';


           data += ' </div>';
       
      }

      setTimeout(function(){
         $("#preview_panel").html(data);
      },1500);
    }

    console.log(data);

    $("#modal_preview_files").modal('show');
   }
    


function show_all_board(elem,type){
  var filter = '&filter=' + $("#filter").val();
  var date_range = $("#date_range");
$.ajax({
  type:"POST",
  url:url,
  data:'action=show_index' + '&type=' + type + '&date_range=' + date_range.val() + filter,
  cache:false,
  success:function(data){
    $("#"+elem).html(data);
  }
});
}

function show_all_announcement(){
  show_all_board('announcement_board','Announcement');
  show_all_board('schedule_board','Schedule');
  show_all_board('activity_board','Activity');
  show_tab_reminder();
  show_deans_board();
}


function show_all_trigger(){
 setInterval(function(){
   show_all_announcement();
  },4000);
}




function startTime() {
  var today = new Date();

  var s = today.getSeconds();
  var h = today.getHours();
  var m = today.getMinutes();
  var ampm = h >= 12 ? 'pm' : 'am';
  h = h % 12;
  h = h ? h : 12; // the hour '0' should be '12'
  m = m < 10 ? '0'+m : m;
  var strTime = h + ':' + m + ' ' + ampm;
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('txt').innerHTML =
  strTime;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}

var video = document.getElementById("myVideo");

// Get the button
var btn = document.getElementById("myBtn");

// Pause and play the video, and change the button text
function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}

var elem = document.getElementById("page-top");

/* When the openFullscreen() function is executed, open the video in fullscreen.
Note that we must include prefixes for different browsers, as they don't support the requestFullscreen method yet */
function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

// openFullscreen();


function show_deans_board(){
    
      $.ajax({
        type:"POST",
        url:url,
        data:'action=show_deans_index',
        cache:false,
        success:function(data){
          console.log('asd');
          $("#deans_board").html(data);
        }
      });
    }



    function show_tab_reminder(){
      var filter = '&filter=' + $("#filter").val();
      $.ajax({
        type:"POST",
        url:url,
        data:'action=show_tab_reminder' + filter,
        cache:false,
        success:function(data){
          // console.log('asd');
          $("#reminder_board").html(data);
        }
      });
    }