var url = '../function/function.php';

function alert_text(alert){
  if (alert == "") {
    $("#alert_text").text(alert);
    $("#alert_text").attr('style', 'display:none');
  }else{
     $("#alert_text").text(alert);
     $("#alert_text").attr('style', 'display:');
  }
 
}

function login(){
  var email_add = $("#email_address");
  var pwd = $("#password");

  if (email_add.val() == "") {
    email_add.focus();
    alert_text('Enter username!');
  }else if (pwd.val() == "") {
    pwd.focus();
    alert_text('Enter password!');

  }else{
    var mydata = 'action=login' + '&email_address=' + email_add.val() + '&password=' + pwd.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
         alert_text('');
      },
      success:function(data){
        // alert(data.trim());
        if (data.trim() == 1 || data.trim() == 2) {
          // admin
          email_add.val('');
          pwd.val('');
          window.location = "../admin/";
          alert_text('');
        }else{
           swal("Oops!","Username or password is incorrect!","error");
           console.log(data.trim());
        }
      }
    });
  }

}
