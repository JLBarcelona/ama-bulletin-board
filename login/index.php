<?php 
session_start();
if (isset($_SESSION['profile_id'])) {
    @header('location:../admin/');
}
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AMA Bulletin Board Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../webroot/admin/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../webroot/admin/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../webroot/admin/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../webroot/admin/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../webroot/img/logo.png">

   
    <link rel="stylesheet" href="../assets/css/sweetalert.css">
    <link rel="stylesheet" href="../assets/css/themes/twitter/twitter.css">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page-holder d-flex align-items-center">
      <div class="container">
        <div class="row align-items-center py-5">
          <div class="col-5 col-lg-7 mx-auto mb-5 mb-lg-0">
             <div id="demo" class="carousel slide" data-ride="carousel" style="padding: 0px;">

                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                  </ul>


                  <!-- The slideshow -->
                  <div class="carousel-inner" style="padding: 0px;">

                    <div class="carousel-item active">
                      <img src="../webroot/img/portfolio/fullsize/1.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
                    </div>
                    <div class="carousel-item">
                     <img src="../webroot/img/portfolio/fullsize/2.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
                    </div>
                    <div class="carousel-item">
                      <img src="../webroot/img/portfolio/fullsize/3.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="carousel-control-prev " href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next " href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>

                </div>
          </div>
          <div class="col-lg-5 px-lg-4">
          <div class="card">
            <div class="card-body">
                <h1 class="text-base text-primary text-uppercase mb-4"></h1>
                  <h2 class="mb-4">Login</h2>
                 
                    <div class="form-group mb-4">
                      <input type="text" name="email" id="email_address" placeholder="Email address" class="form-control border-0 shadow form-control-lg" autocomplete="off">
                      <span class="text-danger mt-2" id="alert_email_address"></span>
                    </div>
                    <div class="form-group mb-4">
                      <input type="password" name="password" id="password" placeholder="Password" class="form-control border-0 shadow form-control-lg text-violet">
                      <span class="text-danger mt-2" id="alert_password"></span>
                    </div>
                  
                    <div class="text-right">
                        <span class="alert alert-danger alert-sm" id="alert_text" style="display: none;"></span>
                         <a href="change_password.php" class="">Forgot Password?</a>
                       <button type="submit" class="btn btn-primary shadow px-5 ml-2" onclick="login();">Log in</button>
                      
                  </div>
            </div>
          </div>
            
          </div>
        </div>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)                 -->
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../webroot/admin/vendor/jquery/jquery.min.js"></script>
    <script src="../webroot/admin/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../webroot/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../webroot/admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../webroot/js/index.js"></script>
    <script src="../webroot/js/tools.js"></script>
    <script src="../assets/js/sweetalert.min.js"></script>
  </body>
</html>