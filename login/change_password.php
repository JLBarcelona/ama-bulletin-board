<?php 
session_start();
if (isset($_SESSION['profile_id'])) {
    @header('location:../admin/');
}
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AMA Bulletin Board Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../webroot/admin/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../webroot/admin/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../webroot/admin/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../webroot/admin/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../webroot/img/logo.png">

   
    <link rel="stylesheet" href="../assets/css/sweetalert.css">

    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <div class="page-holder d-flex align-items-center">
      <div class="container">
        <div class="row ">
          <div class="col-sm-3 ">
          </div>
          <div class="col-sm-6 ">
            <div class="card">
              <div class="card-body">
                  <div class="pr-3 pl-3">
                    <!-- Portfolio Section Heading -->
                    <h3 class="text-center text-uppercase text-secondary mb-2">Forgot Password ?</h3>
                    <hr>
                      <form action="#" method="post" id="formemail">
                        <div class="form-group mb-3">
                          <input type="text" class="form-control" placeholder="Enter your username" id="email" autocomplete="off">
                        </div>
                        <div class="row">
                          <div class="col-8">
                          </div>
                          <!-- /.col -->
                          <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                          </div>
                          <!-- /.col -->
                        </div>
                      </form>

                      <div  id="form_question" style="display: none;">

                        <div class="form-group mb-3 text-center">
                         <h5 class="text-center text-uppercase text-secondary mb-2" id="questionare"></h5>
                        </div>

                        <div class="form-group mb-3">
                          <input type="password" class="form-control" placeholder="Enter your answer" id="sec_answer" autocomplete="off">
                        </div>

                        <div class="row">
                          <div class="col-8">
                          </div>
                          <!-- /.col -->
                          <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" onclick="verify_answer();">Submit</button>
                          </div>
                          <!-- /.col -->
                        </div>
                      </div>

                      <form action="#" method="post" id="form_code" style="display: none;">

                        <div class="form-group mb-3">
                          <input type="password" class="form-control" placeholder="New Password" id="new_password" autocomplete="off">
                        </div>

                        <div class="form-group mb-3">
                          <input type="password" class="form-control" placeholder="Confirm Password" id="c_password" autocomplete="off">
                        </div>

                        <div class="row">
                          <div class="col-8">
                          </div>
                          <!-- /.col -->
                          <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                          </div>
                          <!-- /.col -->
                        </div>
                      </form>
                  </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)                 -->
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="../webroot/admin/vendor/jquery/jquery.min.js"></script>
    <script src="../webroot/admin/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="../webroot/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../webroot/admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../webroot/js/index.js"></script>
    <script src="../webroot/js/tools.js"></script>
    <script src="../assets/js/sweetalert.min.js"></script>
  </body>
</html>

<script type="text/javascript">
  var myid = '';
  var answer = '';

function verify_answer(){
  var sec_answer = $("#sec_answer");
  if (sec_answer.val() == "") {
    sec_answer.focus();
    swal("Oops!","Please enter your answer!","error");
  }else if (sec_answer.val() != answer) {
    sec_answer.focus();
    swal("Oops!","Answer doesn\'t match!","error");
  }else{
    $("#form_question").hide('fast');
    $("#form_code").show('fast');
  }
}

$("#formemail").on('submit', function(e){
  e.preventDefault();
  var email = $("#email");

  if (email.val() == "") {
    email.focus();
    swal("Oops!","username is required!","error");
  }
  else{ 
    var mydata = 'action=request_code'+'&email=' + email.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      dataType:'json',
      beforeSend:function(){
       // swal("Loading","Please Wait...","info");/z
      },
      success:function(data){
        // console.log(data);
        if (data.result == true) {
          // console.log(data.data);
          $("#questionare").text(data.data.question);
          answer = data.data.answer;
          myid = data.data.account_id;
          $("#form_question").show('fast');
          $("#formemail").hide('fast');
          // console.log(data.data.question);
          // console.log(data.data.answer);
        }else{
          swal("Oops!",data.msg,"error");
        }
      }
    });
  }

  });


function alphanumeric() { 
    var txt = document.getElementById("new_password");
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if(txt.value.match(passw)) { 
      // alert('Correct, try another...')
      return true;
    }
    else{ 
      // alert('Wrong...!')
      return false;
    }
  } 


$("#form_code").on('submit', function(e){
  e.preventDefault();
  
 
  var new_password = $("#new_password");
  var c_password = $("#c_password");
  
   if (new_password.val() == "") {
    new_password.focus();
    swal("Oops!","New password is required!","error");
  }
  else if (alphanumeric() == false) {
      swal("Oops!","Password Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters","error");
      new_password.focus();
  }
  else if (c_password.val() == "") {
    c_password.focus();
    swal("Oops!","Confirm your password!","error");
  }
  else if (c_password.val() != new_password.val()) {
    c_password.focus();
    swal("Oops!","Pasword doesn\'t match!","error");
  }
  else{ 
    var mydata = 'action=forgot_password' + '&new_password=' + new_password.val() + '&myid=' + myid;
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
       swal("Loading!","Please Wait...","info");
      },
      success:function(data){
        console.log(data.trim());
        if (data.trim() == 1) {
        swal("Success","Password has been updated!","success");
           setTimeout(function(){
             window.location="./";
           },1500);
        }else{
          swal("Oops!","error!","error");
          console.log(data.trim());
        }
      }
    });
  }

  });
</script>