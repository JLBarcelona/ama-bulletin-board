<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../vendor/vendor/autoload.php';

$id = (isset($_SESSION['profile_id'])) ? $_SESSION['profile_id'] : '';

$auth = get_session($con,$id);

$able = '';

if (isset($_SESSION['profile_id'])) {
	$able = ($auth['user_type'] == 2)? 'none' : ''; 
}

function save($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}

function count_dashboard($con,$type){
	switch ($type) {
		case 'user':
			$sql = "SELECT count(profile_id) from tbl_profile where deleted is null";
			$data = array();
			return count_record($con,$data,$sql,'profile_id');
		break;
		
		case 'post':
			$sql = "SELECT count(post_id) from tbl_announcement where is_deleted = 0";
			$data = array();
			return count_record($con,$data,$sql,'post_id');
		break;

		case 'reminder':
			$sql = "SELECT count(rem_id) from tbl_reminder where is_delete = 0";
			$data = array();
			return count_record($con,$data,$sql,'rem_id');
		break;

		default:
			# code...
		break;
	}
}

function insert($con,$table,$arrs){
		$parameter = array();
		$data = array();
		$dataparam = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key;
			$dataparam[] = ":".$key;
			$data[$key] = $value;
		}

		$sql = "INSERT INTO ".$table."(".implode(",", $parameter).") VALUES(".implode(",", $dataparam).")";

		if (save($con,$data,$sql) > 0) {
			return 1;
		}else{
			return save($con,$data,$sql);
		}
		
	}
	function update($con,$table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key."=:".$key;
			$data[$key] = $value;
		}


		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}


		$sql = "UPDATE ".$table." set ".implode(",", $get_parameter)." WHERE ".$selector;

		if (save($con,$data,$sql) > 0) {
			return 1;
		}else{
			return save($con,$data,$sql);
		}
		
	}
function delete($con,$table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key."=:".$key;
			$data[$key] = $value;
		}


		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}


		$sql = "DELETE FROM ".$table." WHERE ". $selector;

		if (save($con,$data,$sql) > 0) {
			return 1;
		}else{
			return save($con,$data,$sql);
		}
	}


function get_max_id($con){
	$data = array();
	$sql ="SELECT max(profile_id) from tbl_profile";

	return max_record($con,$data,$sql,'profile_id') + 1;
}

// Get count record
function count_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['count('.$count.')'] > 0) {
    	return $row['count('.$count.')'];
    }else{
    	return 0;
    }
}


// get  max record
function max_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['max('.$count.')'] > 0) {
    	return $row['max('.$count.')'];
    }else{
    	return 0;
    }
}


function get_session($con,$id){
	$data = array('id' => $id);
    $sql = "SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where a.profile_id = :id";
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    return $row;
}

function fetch_record($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $prep;
}

function iteration($data){
	if (!empty($data)) {
		return $data;
	}else{
		return '';
	}
}


function check_upload_2x2($con,$id){
	$data = array('id' => $id);

	$sql = "SELECT profile_id from tbl_requirements where profile_id = :id and is_photo = 1";
	
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}


function verify_record($con,$data,$sql){
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}



function userAuth($row){
	$_SESSION['account_id'] = iteration($row['account_id']);
	$_SESSION['profile_id'] = iteration($row['profile_id']);
	$_SESSION['fullname'] = iteration($row['fn'].' '.$row['ln']);
	$_SESSION['firstname'] = iteration($row['fn']);
	$_SESSION['middlename'] = iteration($row['mn']);
	$_SESSION['lastname'] = iteration($row['ln']);
	$_SESSION['birthdate'] = iteration($row['bdate']);	
	$_SESSION['gender'] = iteration($row['gender']);
	$_SESSION['address'] = iteration($row['address']);
	// user types 1 = admin, 2 = staff and 3 = pwd_user
	$_SESSION['user_type'] = iteration($row['user_type']);
	// verifications puposes
	$_SESSION['email'] = iteration($row['email_address']);
	$_SESSION['password'] = iteration($row['password']);
}


function register_account($con,$profile_id,$email,$password_hashed,$access_token,$approve,$status){
	

	$data2 = array('profile_id' => $profile_id,'email' => $email, 'password' => $password_hashed ,'access_token' => $access_token,'user_type' => $status,'date_reg' => date('Y-m-d'), 'approval' => $approve);

	 $query = "INSERT INTO tbl_account (profile_id,email_address,password,access_token,user_type,date_registered,is_approve) VALUES (:profile_id,:email,:password,:access_token,:user_type,:date_reg,:approval);";

	 $result = save($con,$data2,$query);

	 return $result;
}


function GetLastId($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $con->lastInsertId();
}


function get_dean_post_home($row){
	$file_path = 'dean/';

	$files_uploaded = explode('~', $row['is_file_path']);
	$files_count = count($files_uploaded);

	$display = ($files_count > 1)? '' : 'none';
	$types = "Deans Lister";
	?>
	 <div class="card shadow mt-3 mb-1">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" >
          <h5 class="media-heading"><?php echo $types ?></h5>
      </div>
      <!-- Card Body -->
      <div class="card-body" onclick="show_previews('<?php echo $types ?>','<?php echo $row['is_file_path'] ?>','<?php echo  $file_path.$row['profile_id'].'/' ?>');">
        <div class="media-body">
           <div class="row">
           	
           
           <?php for ($i=0; $i < count($files_uploaded); $i++) { ?>
           		<?php 
           				$file_ext = explode('.', $files_uploaded[$i]);
						$extention = strtolower(end($file_ext)); 
				?>

	           	 <?php if (strtoupper($extention) == 'png' || strtoupper($extention) == 'jpg' || strtoupper($extention) == 'jpeg' || strtoupper($extention) == 'PNG' || strtoupper($extention) == 'JPG' || strtoupper($extention) == 'JPEG'): ?>
						<?php if ($files_count > 1): ?>
							<?php if ($files_count == 1): ?>
								<div class="col-sm-12 col-lg-12 ">
									<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px;background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
								</div>


								<?php elseif ($files_count == 2): ?>

										<div class="col-sm-6 col-lg-6 ">
											<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px;background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
										</div>

									<?php else: ?>

											<?php if ($i == 0): ?>
												<div class="col-sm-6 col-lg-6">
													<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
												</div>

											<?php elseif ($i == 1): ?>
												<div class="col-sm-6 col-lg-6 img-text-container hover-more">
													<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>); ">
														<div class="centered h5 p-1 rounded" style="background-color: rgba(0,0,0,0.5); text-shadow: 2px 2px #000;">+<?php echo $files_count - 2 ?> More</div>
												</div>

											<?php endif ?>
										

								<?php endif ?>


								

							<?php else: ?>

								<div class="col-sm-12 col-lg-12">
									 <img src="<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>" class="img-fluid rounded" style="width: 100%;">
								</div>
						<?php endif ?>


						
						
						<?php else: ?>
							<?php if ($files_count == 1): ?>
									<div class="col-sm-6 col-lg-6">
											<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(webroot/img/files.png);">
										<div class="text-sm"><?php echo $files_uploaded[$i] ?></div>
										<br>
									</div>
									<?php elseif ($files_count == 2): ?>
											<div class="col-sm-6 col-lg-6">
													<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(webroot/img/files.png);">
												<div class="text-sm"><?php echo $files_uploaded[$i] ?></div>
												<br>
											</div>
									<?php else: ?>

							<?php endif ?>
						
		
				<?php endif ?>
           	<?php } ?>
           	</div>
        </div>
      </div>
      <div class="card-footer  text-muted">
  	 

  	 <h6 class="m-0 font-weight-bold text-dark float-left"><!-- <img src="webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> --> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>


      <div class="float-right"><br>Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div> </div>
    </div>
	<?php
}



function get_dean_post($row){
	$file_path = '../dean/';

	$files_uploaded = explode('~', $row['is_file_path']);
	$files_count = count($files_uploaded);

	$display = ($files_count > 1)? '' : 'none';
	$types = "Deans Lister";
	?>
	 <div class="card shadow mt-3 mb-1">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" >
          <h5 class="media-heading"><?php echo $types ?></h5>
      </div>
      <!-- Card Body -->
      <div class="card-body" onclick="show_previews('<?php echo $types ?>','<?php echo $row['is_file_path'] ?>','<?php echo  $file_path.$row['profile_id'].'/' ?>');">
        <div class="media-body">
           <div class="row">
           	
           
           <?php for ($i=0; $i < count($files_uploaded); $i++) { ?>
           		<?php 
           				$file_ext = explode('.', $files_uploaded[$i]);
						$extention = strtolower(end($file_ext)); 
				?>

	           	 <?php if (strtoupper($extention) == 'png' || strtoupper($extention) == 'jpg' || strtoupper($extention) == 'jpeg' || strtoupper($extention) == 'PNG' || strtoupper($extention) == 'JPG' || strtoupper($extention) == 'JPEG'): ?>
						<?php if ($files_count > 1): ?>
							<?php if ($files_count == 1): ?>
								<div class="col-sm-12 col-lg-12 ">
									<img src="../webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px;background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
								</div>


								<?php elseif ($files_count == 2): ?>

										<div class="col-sm-6 col-lg-6 ">
											<img src="../webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px;background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
										</div>

									<?php else: ?>

											<?php if ($i == 0): ?>
												<div class="col-sm-6 col-lg-6">
													<img src="../webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
												</div>

											<?php elseif ($i == 1): ?>
												<div class="col-sm-6 col-lg-6 img-text-container hover-more">
													<img src="../webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>); ">
														<div class="centered h5 p-1 rounded" style="background-color: rgba(0,0,0,0.5); text-shadow: 2px 2px #000;">+<?php echo $files_count - 2 ?> More</div>
												</div>

											<?php endif ?>
										

								<?php endif ?>


								

							<?php else: ?>

								<div class="col-sm-12 col-lg-12">
									 <img src="<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>" class="img-fluid rounded" style="width: 100%;">
								</div>
						<?php endif ?>


						
						
						<?php else: ?>
							<?php if ($files_count == 1): ?>
									<div class="col-sm-6 col-lg-6">
											<img src="../webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(webroot/img/files.png);">
										<div class="text-sm"><?php echo $files_uploaded[$i] ?></div>
										<br>
									</div>
									<?php elseif ($files_count == 2): ?>
											<div class="col-sm-6 col-lg-6">
													<img src="../webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(webroot/img/files.png);">
												<div class="text-sm"><?php echo $files_uploaded[$i] ?></div>
												<br>
											</div>
									<?php else: ?>

							<?php endif ?>
						
		
				<?php endif ?>
           	<?php } ?>
           	</div>
        </div>
      </div>
      <div class="card-footer  text-muted">
  	 

  	 <h6 class="m-0 font-weight-bold text-dark float-left"><!-- <img src="webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> --> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>


      <div class="float-right"><br>Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div> </div>
    </div>
	<?php
}

function get_style_post($row){
	$file_path = '../webroot/uploads/';

	$full_path = $file_path.$row['profile_id'].'/'.$row['is_file_path'];
	// print_r($row);

	if ($row['type'] == "Schedule") {
	?>
	 <div class="card shadow mt-2">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
        <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
         <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Option</div>
            	  <a class="dropdown-item" href="#" onclick="edit_post('<?php echo $row['post_id'] ?>','<?php echo $row['title'] ?>','<?php echo $row['content'] ?>','<?php echo $row['type'] ?>','<?php echo $row['is_file_path'] ?>','<?php echo $full_path ?>','<?php echo $row['date_interval'] ?>');"><i class="fa fa-edit"></i> Edit</a>
                  <a class="dropdown-item" href="#" onclick="delete_post('<?php echo $row['post_id'] ?>')"><i class="fa fa-trash"></i> Delete</a>
            </div>
          </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="media-body">
           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
           <p><?php echo $row['content'] ?></p>
           <img src="<?php echo $file_path.$row['profile_id'].'/'.$row['is_file_path'] ?>" class="img-fluid rounded" style="width: 100%;">
        </div>
      </div>
      <div class="card-footer text-right text-muted">
      <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
    </div>
	<?php
	}else if ($row['type'] == "Announcement") {
	?>
	 <div class="card shadow mt-2">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
       <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
       <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-400"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          <div class="dropdown-header">Option</div>
          <a class="dropdown-item" href="#" onclick="edit_post('<?php echo $row['post_id'] ?>','<?php echo $row['title'] ?>','<?php echo $row['content'] ?>','<?php echo $row['type'] ?>','<?php echo $row['is_file_path'] ?>','<?php echo $full_path ?>','<?php echo $row['date_interval'] ?>');"><i class="fa fa-edit"></i> Edit</a>
          <a class="dropdown-item" href="#" onclick="delete_post('<?php echo $row['post_id'] ?>')"><i class="fa fa-trash"></i> Delete</a>
        </div>
      </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="media-body">
           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
           <p><?php echo $row['content'] ?></p>
        </div>
      </div>
       <div class="card-footer text-right text-muted">
       <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
    </div>
	<?php
	}else if ($row['type'] == "Activity") {
		?>
		 <div class="card shadow mt-2">
		      <!-- Card Header - Dropdown -->
		      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
		        <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
		        <div class="dropdown no-arrow">
	                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                  <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-400"></i>
	                </a>
	                <div class="dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
	                  <div class="dropdown-header">Option</div>
	                  <a class="dropdown-item" href="#" onclick="edit_post('<?php echo $row['post_id'] ?>','<?php echo $row['title'] ?>','<?php echo $row['content'] ?>','<?php echo $row['type'] ?>','<?php echo $row['is_file_path'] ?>','<?php echo $full_path ?>','<?php echo $row['date_interval'] ?>');"><i class="fa fa-edit"></i> Edit</a>
	                  <a class="dropdown-item" href="#" onclick="delete_post('<?php echo $row['post_id'] ?>')"><i class="fa fa-trash"></i> Delete</a>
	                </div>
	              </div>
		      </div>
		      <!-- Card Body -->
		      <div class="card-body">
		        <div class="media-body">
		           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
		           <p><?php echo $row['content'] ?></p>
		        </div>
		      </div>
		      <div class="card-footer text-right text-muted">
		      <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
		    </div>
		<?php
	}else{
		?>
		<center><div class="text-muted mt-5">**** Nothing Follows ****</div></center>
		<?php
	}
}


function get_style_post_no_drop($row){
	$file_path = '../webroot/uploads/';

	// print_r($row);

	if ($row['type'] == "Schedule") {
	?>
	 <div class="card shadow mt-2">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
        <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
   
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="media-body">
           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
           <p><?php echo $row['content'] ?></p>
           <img src="<?php echo $file_path.$row['profile_id'].'/'.$row['is_file_path'] ?>" class="img-fluid rounded" style="width: 100%;">
        </div>
      </div>
      <div class="card-footer text-right text-muted">
      <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
    </div>
	<?php
	}else if ($row['type'] == "Announcement") {
	?>
	 <div class="card shadow mt-2">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
       <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>

      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="media-body">
           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
           <p><?php echo $row['content'] ?></p>
        </div>
      </div>
       <div class="card-footer text-right text-muted">
       <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
    </div>
	<?php
	}else if ($row['type'] == "Activity") {
		?>
		 <div class="card shadow mt-2">
		      <!-- Card Header - Dropdown -->
		      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
		        <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
		      </div>
		      <!-- Card Body -->
		      <div class="card-body">
		        <div class="media-body">
		           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
		           <p><?php echo $row['content'] ?></p>
		        </div>
		      </div>
		      <div class="card-footer text-right text-muted">
		      <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
		    </div>
		<?php
	}else{
		?>
		<center><div class="text-muted mt-5">**** Nothing Follows ****</div></center>
		<?php
	}
}


function get_style_post_no_drop_home($row){
	$file_path = 'uploads/';

	// print_r($row);
	$content = (!empty($row['content']))? $row['content'] : '~~~-';

	$data_con = explode('~', $content);
	$why = $data_con[1];
	$where = $data_con[2];
	$when = $data_con[3];

	$files_uploaded = explode('~', $row['is_file_path']);
	$files_count = count($files_uploaded);

	$display = ($files_count > 1)? '' : 'none';

	$typess = ($row['type'] == 'Announcement') ? 'Academic Calendar' : $row['type'];

	$types = ($typess == 'Activity')? 'Program' : $typess;

	
	?>
	 <div class="card shadow mt-3 mb-1">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" >
          <h5 class="media-heading"><?php echo $types ?></h5>
      </div>
      <!-- Card Body -->
      <div class="card-body" onclick="show_previews('<?php echo $types ?>','<?php echo $row['is_file_path'] ?>','<?php echo  $file_path.$row['profile_id'].'/' ?>');">
        <div class="media-body">
           <?php if ($types == 'Schedule'): ?>
           	  <h3 class="media-heading m-1 h4"><b><?php echo $row['title'] ?></b></h3>
	           <p class="m-1"><?php echo $why .' on '.$where.' @ '.$when ?></p>

           <?php endif ?>
           <div class="row">
           	
           
           <?php for ($i=0; $i < count($files_uploaded); $i++) { ?>
           		<?php 
           				$file_ext = explode('.', $files_uploaded[$i]);
						$extention = strtolower(end($file_ext)); 
				?>

	           	 <?php if (strtoupper($extention) == 'png' || strtoupper($extention) == 'jpg' || strtoupper($extention) == 'jpeg' || strtoupper($extention) == 'PNG' || strtoupper($extention) == 'JPG' || strtoupper($extention) == 'JPEG'): ?>
						<?php if ($files_count > 1): ?>
							<?php if ($files_count == 1): ?>
								<div class="col-sm-12 col-lg-12 ">
									<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px;background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
								</div>


								<?php elseif ($files_count == 2): ?>

										<div class="col-sm-6 col-lg-6 ">
											<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px;background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
										</div>

									<?php else: ?>

											<?php if ($i == 0): ?>
												<div class="col-sm-6 col-lg-6">
													<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>);">
												</div>

											<?php elseif ($i == 1): ?>
												<div class="col-sm-6 col-lg-6 img-text-container hover-more">
													<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>); ">
														<div class="centered h5 p-1 rounded" style="background-color: rgba(0,0,0,0.5); text-shadow: 2px 2px #000;">+<?php echo $files_count - 2 ?> More</div>
												</div>

											<?php endif ?>
										

								<?php endif ?>


								

							<?php else: ?>

								<div class="col-sm-12 col-lg-12">
									 <img src="<?php echo $file_path.$row['profile_id'].'/'.$files_uploaded[$i] ?>" class="img-fluid rounded" style="width: 100%;">
								</div>
						<?php endif ?>


						
						
						<?php else: ?>
							<?php if ($files_count == 1): ?>
									<div class="col-sm-6 col-lg-6">
											<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(webroot/img/files.png);">
										<div class="text-sm"><?php echo $files_uploaded[$i] ?></div>
										<br>
									</div>
									<?php elseif ($files_count == 2): ?>
											<div class="col-sm-6 col-lg-6">
													<img src="webroot/img/img.png" class="img-thumbnail  post_bg" style="width: 280px; height:280px; background-image: url(webroot/img/files.png);">
												<div class="text-sm"><?php echo $files_uploaded[$i] ?></div>
												<br>
											</div>
									<?php else: ?>

							<?php endif ?>
						
		
				<?php endif ?>
           	<?php } ?>
           	</div>
        </div>
      </div>
      <div class="card-footer  text-muted">
  	 

  	 <h6 class="m-0 font-weight-bold text-dark float-left"><!-- <img src="webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> --> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>


      <div class="float-right"><br>Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div> </div>
    </div>
	<?php

}


function get_style_post_deleted($row){
	$file_path = '../webroot/uploads/';

	$full_path = $file_path.$row['profile_id'].'/'.$row['is_file_path'];
	// print_r($row);

	if ($row['type'] == "Schedule") {
	?>
	 <div class="card shadow mt-2">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
        <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
         <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
              <div class="dropdown-header">Option</div>
            	   <a class="dropdown-item" href="#" onclick="recover_post('<?php echo $row['post_id'] ?>')"><i class="fa fa-recycle"></i> Recover</a>
                  <a class="dropdown-item" href="#" onclick="delete_post_from_history('<?php echo $row['post_id'] ?>')"><i class="fa fa-trash"></i> Delete</a>
            </div>
          </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="media-body">
           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
           <p><?php echo $row['content'] ?></p>
           <img src="<?php echo $file_path.$row['profile_id'].'/'.$row['is_file_path'] ?>" class="img-fluid rounded" style="width: 100%;">
        </div>
      </div>
      <div class="card-footer text-right text-muted">
      <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
    </div>
	<?php
	}else if ($row['type'] == "Announcement") {
	?>
	 <div class="card shadow mt-2">
      <!-- Card Header - Dropdown -->
      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
       <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
       <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-400"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          <div class="dropdown-header">Option</div>
           <a class="dropdown-item" href="#" onclick="recover_post('<?php echo $row['post_id'] ?>')"><i class="fa fa-recycle"></i> Recover</a>
          <a class="dropdown-item" href="#" onclick="delete_post_from_history('<?php echo $row['post_id'] ?>')"><i class="fa fa-trash"></i> Delete</a>
        </div>
      </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="media-body">
           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
           <p><?php echo $row['content'] ?></p>
        </div>
      </div>
       <div class="card-footer text-right text-muted">
       <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
    </div>
	<?php
	}else if ($row['type'] == "Activity") {
		?>
		 <div class="card shadow mt-2">
		      <!-- Card Header - Dropdown -->
		      <div class="card-header bg-white py-3 d-flex flex-row align-items-center justify-content-between" style="border-bottom:0px;">
		        <h6 class="m-0 font-weight-bold text-dark "><img src="../webroot/img/admin.jpg" class="media-object rounded" style="width:30px"> <?php echo $row['fn'].' '.$row['ln'] ?><br> <small class="text-muted text-primary"><?php echo $row['position'] ?></small></h6>
		        <div class="dropdown no-arrow">
	                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                  <i class="fas fa-ellipsis-h fa-sm fa-fw text-gray-400"></i>
	                </a>
	                <div class="dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
	                  <div class="dropdown-header">Option</div>
	                 <a class="dropdown-item" href="#" onclick="recover_post('<?php echo $row['post_id'] ?>')"><i class="fa fa-recycle"></i> Recover</a>
	                  <a class="dropdown-item" href="#" onclick="delete_post_from_history('<?php echo $row['post_id'] ?>')"><i class="fa fa-trash"></i> Delete</a>
	                </div>
	              </div>
		      </div>
		      <!-- Card Body -->
		      <div class="card-body">
		        <div class="media-body">
		           <h6 class="media-heading"><?php echo $row['title'] ?></h6>
		           <p><?php echo $row['content'] ?></p>
		        </div>
		      </div>
		      <div class="card-footer text-right text-muted">
		      <span class="float-left"><b><?php echo $row['type'] ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
		    </div>
		<?php
	}else{
		?>
		<center><div class="text-muted mt-5">**** Nothing Follows ****</div></center>
		<?php
	}
}

function format_date($format,$date){
	return date ($format,strtotime($date));
}

function delete_query($con,$data,$sql){
	// sample
	// DELETE from tbl_employee WHERE id=:id
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}


function send_email_all($name,$email,$code){
	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {
	    //Server settings
     	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->SMTPDebug = false; 								  // Enable verbose debug output
	    $mail->isSMTP(); 									  // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  				      // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true; 							  // Enable SMTP authentication
        $mail->Username = 'hermo12345six@gmail.com'; 				  // SMTP username
	    $mail->Password = 'greatwhite18!';                           // SMTP password
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->setFrom('hermo12345six@gmail.com', 'AMA Bulletin Board System.');
	    $mail->addAddress($email, $name);     // Add a recipient
	 
	    $messages = 'Hi, '.$name.'!<br>';
	    $messages .= 'Copy Your Password Verification Code.<br><br>';
	    $messages .= '============================<br>';
	    $messages .= 'Code:'.$code.'<br>';
	    $messages .= '============================<br>';

	    // Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = '(AMA Bulletin Board System) Forgot Password Code!';
	    $mail->Body = $messages;
	    $mail->AltBody = '';

		if ($mail->send()) {
			echo  1;
		}

	} catch (Exception $e) {
	    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}

?>



