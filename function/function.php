<?php 
session_start();
include("config.php");
include("helper.php");
// include("sms_helper.php");
date_default_timezone_set('Asia/Manila');

$id = (isset($_SESSION['profile_id']))? $_SESSION['profile_id'] : '';


$action  = $_POST['action'];
switch ($action) {
	case 'show_deans_lister_home':
		$data = array();

		$sql = "SELECT * from tbl_dean_list order by average desc";

			$result = fetch_record($con,$data,$sql);
			$i = 0;
			?>
			<table class="table bg-white table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>USN</th>
						<th>NAME</th>
						<th>COURSE</th>
						<th>YEAR</th>
						<th>SECTION</th>
						<th>GPA</th>
					</tr>
				</thead>
				<tbody>
			<?php

			while ($row = $result->fetch()) {
				$i++;
				?>
				<tr>
					<td class="text-center" width="5"><?php echo $i ?></td>
					<td class="text-capitalize"><?php echo $row['usn']; ?></td>
					<td><?php echo ucfirst($row['ln']).', '.ucfirst($row['fn']).' ' .ucfirst($row['mn'][0]).'.' ?></td>
					<td class="text-capitalize"><?php echo $row['course']; ?></td>
					<td ><?php echo $row['year'] ?></td>
					<td class=""><?php echo $row['section'] ?></td>
					<td class=""><?php echo $row['average'] ?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
			</table>
			<?php
	break;
	


	case 'show_deans_post':	
		$data = array('myid' => $id);
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_dean_post b on a.profile_id=b.profile_id where b.profile_id=:myid and b.is_deleted = 0 order by dean_post_id desc";
		$result = fetch_record($con,$data,$sql);

		
		while ($row  = $result->fetch()) {
			echo get_dean_post($row);
		}
	break;

	case 'show_deans_index':	
		$data = array('myid' => $id);
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_dean_post b on a.profile_id=b.profile_id where b.is_deleted = 0 order by dean_post_id desc";
		$result = fetch_record($con,$data,$sql);

		
		while ($row  = $result->fetch()) {
			echo get_dean_post_home($row);
		}
	break;

	case 'show_tab_reminder':
	$data = array();

	$sql = "SELECT * from tbl_reminder order by rem_id desc";

		$result = fetch_record($con,$data,$sql);
		$i = 0;
		while ($row = $result->fetch()) {
			$style =  (strtotime($row['expiry']) >= strtotime(date('Y-m-d')))? 'display:' : 'display:none';
			$active_stat = (strtotime($row['expiry']) >= strtotime(date('Y-m-d')))? 'Active' : 'Inactive';

			$status = '';

			if (strtotime($row['expiry']) >= strtotime(date('Y-m-d'))) {
				$status = 'active';
			}else if (strtotime($row['expiry']) < strtotime(date('Y-m-d'))) {
				$status = 'inactive';
			}else{
				$status = 'all';
			}
			$i++;


			$style_display = '';

			?>
			<div class="card" style="<?php echo $style ?>">
				<div class="card-header">Reminder</div>
				<div class="card-body">
					<?php echo $row['reminder_msg']; ?>
				</div>
				<div class="card-footer text-right text-muted">
    			  <span class="float-left"><b><?php ?></b></span> Posted on <?php echo format_date('M d Y, H:i A',$row['date_posted']) ?></div>
			</div>
			<?php
		}
	break;

	// Login backend Script
	case 'add_reminder':
	$rem_id = $_POST['rem_id'];
	$reminder_msg = $_POST['reminder_msg'];
	$expiry = $_POST['expiry'];
		
	  if (!empty($rem_id)) {
	  	  $data = array('rem_id' => $rem_id, 'reminder_msg' => $reminder_msg, 'expiry' => $expiry);
		  $response = update($con,'tbl_reminder',$data);
		  if($response > 0){
		    echo 2;
		  }
	  }else{
	  	  $data = array( 'reminder_msg' => $reminder_msg, 'expiry' => $expiry, 'date_posted' => date('Y-m-d'));
		  $response = insert($con,'tbl_reminder',$data);
		  if($response > 0){
		    echo 1;
		  }
	  }
	break;

	case 'import_excel_files':
	require_once('../vendor/php-excel-reader/excel_reader2.php');
	require_once('../vendor/SpreadsheetReader.php');
	
		if (!empty($_FILES['file_import'])) {
		$files = $_FILES['file_import'];
		$array = array();
		// $import_type = $_POST['import_type'];
		// $user_import_type = $_POST['user_import_type'];
		// var_dump($files);
		// echo $import_type;
		$message = '';
		$status = false;


		$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
	  
		  if(in_array($_FILES["file_import"]["type"],$allowedFileType)){

		        $targetPath = '../excel/'.$_FILES['file_import']['name'];
		        move_uploaded_file($_FILES['file_import']['tmp_name'], $targetPath);
		        
		        $Reader = new SpreadsheetReader($targetPath);
		        
		        $sheetCount = count($Reader->sheets());
		        for($i=0;$i<$sheetCount;$i++)
		        {
		            
		            $Reader->ChangeSheet($i);
		            
		             foreach ($Reader as $Row){
			          	include("import_helper.php");		          	
		       	  }
		     }
		  }

		}


		  if ($status == true) {
		  	echo 1;
		  }
		# code...
	break;


	case 'save_deans_lister':
	$dean_id = $_POST['dean_id'];
	$f_name = $_POST['f_name'];
	$m_name = $_POST['m_name'];
	$l_name = $_POST['l_name'];
	$dean_course = $_POST['dean_course'];
	$dean_year = $_POST['dean_year'];
	$dean_section = $_POST['dean_section'];
	$average = $_POST['average'];
	$usn = $_POST['usn'];

	$data_ver = array('dean_id' => $dean_id, 'usn' => $usn);
	$verify_record = "SELECT * from tbl_dean_list where dean_id!=:dean_id and usn=:usn";

	if (verify_record($con,$data_ver,$verify_record) > 0) {
		echo 404;
	}else{
		if (!empty($dean_id)) {
			 $data = array('dean_id' => $dean_id,'usn' => $usn, 'fn' => $f_name, 'mn' => $m_name, 'ln' => $l_name, 'course' => $dean_course, 'year' => $dean_year, 'section' => $dean_section, 'average' => $average, 'date_inserted' => date('Y-m-d H:i:s A'));
			  
			  $response = update($con,'tbl_dean_list',$data);

			  if($response > 0){
			    echo 2;
			  }
		}else{
			 $data = array('usn' => $usn, 'fn' => $f_name, 'mn' => $m_name, 'ln' => $l_name, 'course' => $dean_course, 'year' => $dean_year, 'section' => $dean_section, 'average' => $average, 'date_inserted' => date('Y-m-d H:i:s A'));
			  
			  $response = insert($con,'tbl_dean_list',$data);

			  if($response > 0){
			    echo 1;
			  }	
		}
	}
	break;



	case 'login':
		$email_add = $_POST['email_address'];
		$pwd = $_POST['password'];

		$data = array('email' => $email_add);

		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where b.email_address = :email and b.deleted is null";
		
		$prep = $con->prepare($sql);
		$prep->execute($data);

		if ($prep->rowCount() > 0) {
			$row = $prep->fetch();
			if (password_verify($pwd, $row['password'])) {
				userAuth($row);
				echo $row['user_type'];
			}else{
				echo 0;
			}

		}else{
			echo 0;
		}

	break;

	case 'forgot_password':

		$myid = $_POST['myid'];
		$pwd = $_POST['new_password'];

		$datas = array('myid' => $myid);
		$sqls = "SELECT * from tbl_account where verification_code=:code";

		$new_password = password_hash($pwd, PASSWORD_DEFAULT);
		$data = array('password' => $new_password, 'id' => $myid);
		$sql = "UPDATE tbl_account set password=:password where account_id=:id";

		if (save($con,$data,$sql) > 0) {
			echo 1;
		}

		

	break;

	case 'request_code':
		$email = $_POST['email'];
		$code ='';


		$data = array('email' => $email);		
		$sql = "SELECT a.*,b.* from tbl_account a left join tbl_profile b on a.profile_id=b.profile_id where a.email_address=:email";

		if (verify_record($con,$data,$sql) > 0) {
			$result = fetch_record($con,$data,$sql);

		   $row = $result->fetch();

		   $arr = array('question' => $row['sec_question'], 'answer' => $row['sec_answer'], 'account_id' => $row['account_id']);

		   $data = array('result' => true, 'data' => $arr);

		   echo json_encode($data);
		   
		}else{
			  $data = array('result' => false, 'msg' => 'Undefined username!');

	  		  echo json_encode($data);
		}

	break;

	case 'execute_db':
		$data = array();
		$file_path = $_POST['file_path'];
		$file_data = file($file_path);
		$output = '';
		$count = 0;


		foreach ($file_data as $row) {
			$start_char = substr(trim($row), 0, 2);
			if ($start_char != '--' || $start_char != '/*' || $start_char != '//' || $start_char != '') {
				$output = $output . $row; 
				$end_char = substr(trim($row), -1, 1);
				if ($end_char == ';') {	
					if (save($con,$data,$output) != 1) {
						$count++;
					}
				}
			}
		}

		if ($count > 0) {
			echo $count;
		}else{
			echo 0;
		}

	break;

	case 'backup_db':
		$db_name = 'db_ama';
		$show_data = array();
		$sql_tbl = "SHOW TABLES";
		$result = fetch_record($con,$show_data,$sql_tbl);

		$output = "SET FOREIGN_KEY_CHECKS=0;";

		while ($tbls = $result->fetch()) {
			$tables = $tbls['Tables_in_'.$db_name];

			$data =array();
			$sql = "SHOW CREATE TABLE ".$tables."";
			$res = fetch_record($con,$data,$sql);
			$row = $res->fetch();

			 $output .= "DROP TABLE IF EXISTS `".$tables."`;";
			 $output .= "". $row['Create Table'] .";";
			 // echo $output;

			 $data_record = array();
			 $sql_record = "SELECT * from ".$tables."";
			 $data_result = fetch_record($con,$data_record,$sql_record);
			
			 while ($datas = $data_result->fetch()) {
			 	 $tbl_collumn = array_keys($datas);
				 $tbl_data = array_values($datas);

				 // echo $tbl_data[1];
				 // var_dump($tbl_data);
				 $str = implode("','", $tbl_data);
				 $output .= "INSERT INTO $tables (";
				 $output .= "". implode(', ', $tbl_collumn) .") VALUES (";
				 $output .= "'".str_replace("''", 'null', $str)."');";
			 }
			
		}
		// echo $output;
		$file_directory = '../webroot/database_backup';
		$filename = $file_directory."/db_ama_".date('Y-m-d').'.sql';
		$file_handle = fopen($filename, 'w');
		fwrite($file_handle, $output);
		if (fclose($file_handle)) {
			echo 1;
		}
			
	break;

	case 'database_files':
		$file_directory = '../webroot/database_backup';
			$file =	scandir($file_directory);
			foreach ($file as $key => $value) {
				if ($key != 0 && $key != 1) {
					?>
					<tr>
						<td><?php echo $value ?></td>
						<td class="text-center">
							<button class="btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
							<div class="dropdown-menu">
								<a download="" href="<?php echo $file_directory.'/'.$value ?>" class="dropdown-item"><i class="fa fa-download text-primary"></i> Download</a>

								<a href="#" class="dropdown-item" onclick="execute_backup('<?php echo $value ?>','<?php echo $file_directory.'/'.$value ?>');"><i class="fa fa-recycle text-success"></i> Restore</a>
								<!-- <a href="cores/delete.php?url=<?php echo '../'.$file_directory.'/'.$value ?>" class="dropdown-item">
									<i class="fa fa-trash text-danger"></i> Delete</a> -->
							</div>
							
						</td>
					</tr>
					<?php
				}
			}
		break;

	case 'show_announcement':
		// $type = $_POST['type'];
		$data = array();
		$sql = "SELECT * from tbl_reminder where is_delete = 0 order by rem_id desc LIMIT 20";
		$result = fetch_record($con,$data,$sql);
		$i = 0;
		$mms = '';

		if (verify_record($con,$data,$sql) > 0) {
			# code...
		
		while ($row  = $result->fetch()) {
			$i++;

			if ($i == 1) {
				$mms = 'active';	
			}else{
				$mms = '';
			}

			?>
			<div class="carousel-item <?php echo $mms ?> ">
              <div class="card animated fadeIn post-wall-rem text-left">
              	<div class="card-header"><img src="webroot/img/bell.png" width="20"> Reminder</div>
                  <div class="card-body p-2">
                    <?php echo $row['reminder_msg'] ?>
                  </div>
                  <div class="card-footer"></div>
              </div>
            </div>

       
			<?php
		}
	}else{
	?>
		<img src="webroot/img/ama_pending.jpg" class="img-thumbnail">
	<?php
	}
	break;

	case 'show_index':
		$type = $_POST['type'];
		$date_range = $_POST['date_range'];

		$ndate = format_date('Ymd',$date_range);
		$where = '';
		if (!empty($date_range)) {
			$where = "and DATE_FORMAT(b.date_posted,'%Y%m%d') = '$ndate' ";
		}

		$data = array('type' => $type);
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_announcement b on a.profile_id=b.profile_id where b.type=:type and b.is_deleted = 0 ".$where." order by post_id desc LIMIT 20";
		$result = fetch_record($con,$data,$sql);

		?>
		<div class="row">
		<div class="col-sm-3 col-lg-3"></div>
			<div class="col-sm-6 col-lg-6">
				<?php 
					while ($row  = $result->fetch()) {
						?>
						
						<?php

						if (strtotime(date('Y-m-d')) > strtotime($row['date_interval']) &&  ($date_range == "" || $date_range == null)) {
							# code...
						}else{
							get_style_post_no_drop_home($row);
						}
						// get_style_post_no_drop_home($row);
						?>
						
						
						<?php	
					}
				 ?>
		</div>
		<div class="col-sm-3 col-lg-3"></div>
		</div>
		<?php

	
	break;

	case 'delete_file':
		$file_path = $_POST['file_path'];
		if (unlink($file_path)) {
			echo 1;
		}
	break;

	case 'save_announcement':
		$type = (isset($_POST['type']))? $_POST['type'] : '';
		$title = (isset($_POST['title']))? $_POST['title'] : '';
		$content = (isset($_POST['content']))? $_POST['content'] : '';
		$file_path = (isset($_POST['file_path']))? $_POST['file_path'] : '';
		$post_id = (isset($_POST['post_id']))? $_POST['post_id'] : '';
		$exp_date = (isset($_POST['exp_date']))? $_POST['exp_date'] : '';

		$data = array();
		$sql = "";

		

		if (!empty($post_id)) {
			$data = array('post_id' => $post_id,
				 		 'title' => $title,
						 'content' => $content,
						 'type' => $type,
						 'file_path' => $file_path,
						 'exp_date' => $exp_date,
						 'date_edited' => date('Y-m-d H:i:s a'));

			$sql = "UPDATE tbl_announcement set title=:title,content=:content,type=:type,is_file_path=:file_path,date_interval=:exp_date,date_edited=:date_edited where post_id=:post_id";

				$result = save($con,$data,$sql);
				echo $result;
		}else{
				$data = array('profile_id' => $id,
				 		 'title' => $title,
						 'content' => $content,
						 'type' => $type,
						 'file_path' => $file_path,
						 'exp_date' => $exp_date,
						 'date_posted' => date('Y-m-d H:i:s'));

			$sql = "INSERT INTO tbl_announcement (profile_id,title,content,type,is_file_path,date_interval,date_posted) VALUES(:profile_id,:title,:content,:type,:file_path,:exp_date,:date_posted)";

				$result = save($con,$data,$sql);
				echo $result;
		}
		
		// Result from function save return 1 if it's true
	break;

	case 'save_announcement_dean':
		$file_path = (isset($_POST['file_path']))? $_POST['file_path'] : '';
		$post_id = (isset($_POST['post_id']))? $_POST['post_id'] : '';
		

		$data = array();
		$sql = "";

		if (!empty($post_id)) {
			$data = array('post_id' => $post_id,
				 		 'title' => $title,
						 'content' => $content,
						 'type' => $type,
						 'file_path' => $file_path,
						 'exp_date' => $exp_date,
						 'date_edited' => date('Y-m-d H:i:s a'));

			$sql = "UPDATE tbl_dean_post set is_file_path=:file_path,date_edited=:date_edited where post_id=:post_id";

				$result = save($con,$data,$sql);
				echo $result;
		}else{
				$data = array('profile_id' => $id,
						 'file_path' => $file_path,
						 'date_posted' => date('Y-m-d H:i:s'));

			$sql = "INSERT INTO tbl_dean_post (profile_id,is_file_path,date_posted) VALUES(:profile_id,:file_path,:date_posted)";

				$result = save($con,$data,$sql);
				echo $result;
		}
		
		// Result from function save return 1 if it's true
	break;


	case 'delete_post':
		$postid = $_POST['id'];

		$data = array('id' => $postid, 'is_deleted' => 1);
		$sql = "UPDATE tbl_announcement set is_deleted=:is_deleted where post_id=:id";

		echo save($con,$data,$sql);

	break;
	
	case 'show_all_post':
		
		$data = array('myid' => $id);
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_announcement b on a.profile_id=b.profile_id where b.profile_id=:myid and b.is_deleted = 0 order by post_id desc";
		$result = fetch_record($con,$data,$sql);

		
		while ($row  = $result->fetch()) {
			if (strtotime(date('Y-m-d')) > strtotime($row['date_interval'])) {
				# code...
			}else{
			echo get_style_post($row);
		}
		}
		
	break;


	case 'show_post_other':
		$data = array('myid' => $id);
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_announcement b on a.profile_id=b.profile_id where b.profile_id!=:myid and b.is_deleted = 0 order by post_id desc";
		$result = fetch_record($con,$data,$sql);

		

		while ($row  = $result->fetch()) {
			if ($auth['user_type'] == 1) {
				if (strtotime(date('Y-m-d')) > strtotime($row['date_interval'])) {
				# code...
				}else{
				echo get_style_post($row);
				}
			}else{
				if (strtotime(date('Y-m-d')) > strtotime($row['date_interval'])) {
				# code...
				}else{
				get_style_post_no_drop($row);
				}
			}
		
		}
	break;

	case 'show_accounts':
		$data = array();
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where b.user_type=2";
		$result = fetch_record($con,$data,$sql);

		while ($row = $result->fetch()) {
			$mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
			$name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 
			?>
			<tr>
				<td class="text-capitalize"><?php echo $name ?></td>
				<td><?php echo $row['gender'] ?></td>
				<td><?php echo $row['email_address'] ?></td>
				<td class="text-center"><?php echo (!empty($row['deleted']))? 'Deactivate' : 'Activate'; ?></td>
				<td class="text-center">
					<button class="btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
					<div class="dropdown-menu">
					<button class="dropdown-item" onclick="edit('<?php echo $row['profile_id'] ?>','<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['email_address'] ?>','','<?php echo $row['department'] ?>','<?php echo $row['position'] ?>'); onedit();"><i class="fa fa-edit text-success"></i> Edit</button>
					<button class="dropdown-item" onclick="delete_account('<?php echo $row['profile_id'] ?>','<?php echo $name ?>',0);"><i class="fa fa-thumbs-up text-info"></i> Activate</button>
					<button class="dropdown-item" onclick="delete_account('<?php echo $row['profile_id'] ?>','<?php echo $name ?>',1);"><i class="fa fa-thumbs-down text-danger"></i> Deactivate</button>
					</div>
					
				</td>
			</tr>
			<?php
		}

	break;

	case 'show_reminder':
	$data = array();

	$sql = "SELECT * from tbl_reminder order by rem_id desc";

		$result = fetch_record($con,$data,$sql);
		$i = 0;
		while ($row = $result->fetch()) {
			$style =  (strtotime($row['expiry']) >= strtotime(date('Y-m-d')))? 'opacity:1' : 'opacity:0.5';
			$active_stat = (strtotime($row['expiry']) >= strtotime(date('Y-m-d')))? 'Active' : 'Inactive';

			$status = '';

			if (strtotime($row['expiry']) >= strtotime(date('Y-m-d'))) {
				$status = 'active';
			}else if (strtotime($row['expiry']) < strtotime(date('Y-m-d'))) {
				$status = 'inactive';
			}else{
				$status = 'all';
			}
			$i++;


			$style_display = ''; 

			?>
			<tr style="<?php echo $status ?>" >
				<td class="text-center" width="5" style="<?php echo $style ?>"><?php echo $i ?></td>
				<td style="<?php echo $style ?>" class="text-capitalize"><?php echo $row['reminder_msg']; ?></td>
				<td style="<?php echo $style ?>"><?php echo date('Y/m/d', strtotime($row['expiry'])) ?></td>
				<td style="<?php echo $style ?>" class="text-center"><?php echo $active_stat ?></td>
				<td class="text-center">
					<button class="btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
					<div class="dropdown-menu">
						<button class="dropdown-item" onclick="delete_reminder('<?php echo $row['rem_id'] ?>','<?php echo $row['reminder_msg'] ?>')"><i class="fa fa-trash text-danger"></i> Delete</button>
						<button class="dropdown-item" onclick="edit_reminder('<?php echo $row['rem_id'] ?>','<?php echo $row['reminder_msg'] ?>','<?php echo date('Y-m-d', strtotime($row['expiry'])) ?>');"><i class="fa fa-edit text-success"></i> Edit</button>
					</div>
					
				</td>
			</tr>
			<?php
		}
	break;

	case 'show_dean_lister':
		$data = array();

		$sql = "SELECT * from tbl_dean_list order by average desc";

			$result = fetch_record($con,$data,$sql);
			$i = 0;
			while ($row = $result->fetch()) {
				$i++;
				?>
				<tr  >
					<td class="text-center" width="5"><?php echo $i ?></td>
					<td class="text-capitalize"><?php echo $row['usn']; ?></td>
					<td><?php echo ucfirst($row['ln']).', '.ucfirst($row['fn']).' ' .ucfirst($row['mn'][0]).'.' ?></td>
					<td class="text-capitalize"><?php echo $row['course']; ?></td>
					<td ><?php echo $row['year'] ?></td>
					<td class=""><?php echo $row['section'] ?></td>
					<td class=""><?php echo $row['average'] ?></td>
					<td class="">
						<button class="btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
						<div class="dropdown-menu">
							<button class="dropdown-item" onclick="edit_dean('<?php echo $row['fn'] ?>','<?php echo $row['mn'] ?>','<?php echo $row['ln'] ?>','<?php echo $row['course'] ?>','<?php echo $row['year'] ?>','<?php echo $row['section'] ?>','<?php echo $row['average'] ?>','<?php echo $row['usn'] ?>','<?php echo $row['dean_id'] ?>');"><i class="fa fa-edit text-success"></i> Edit</button>
							<button class="dropdown-item" onclick="delete_dean_list('<?php echo $row['dean_id'] ?>', '<?php echo ucfirst($row['ln']).', '.ucfirst($row['fn']).' ' .ucfirst($row['mn'][0]).'.' ?>')"><i class="fa fa-trash text-danger"></i> Delete</button>
						</div>
						
					</td>
				</tr>
				<?php
			}
	break;

	case 'save_account_user':
		$profid = $_POST['profid'];
		$fn = $_POST['fn'];
		$mn = $_POST['mn'];
		$ln = $_POST['ln'];
		$gender = $_POST['gender'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$department = $_POST['department'];
		$position = $_POST['position'];
		$sec_question = $_POST['sec_question'];
		$sec_answer = $_POST['sec_answer'];

		
		
		$data = array('email' => $email , 'profid' => $profid);
		$verify_record = verify_record($con,$data,"SELECT * from tbl_account where email_address=:email and profile_id !=:profid");

		// update
		$account_entry = array();
		$update = "";
		$result = 0;

		if ($verify_record > 0) {
			echo 202;
		}else{

			if (!empty($profid)) {
				$entry = array('profid' => $profid,'fn' => $fn,'mn' => $mn,'ln' => $ln,'gender' => $gender,'department' => $department,'position' => $position);
				$sql = "UPDATE tbl_profile set fn=:fn,mn=:mn,ln=:ln,gender=:gender,department=:department,position=:position where profile_id=:profid";
				
				if (save($con,$entry,$sql) > 0) {
					if (!empty($password) || !empty($sec_question) || !empty($sec_answer)) {

						
							$password_hash = password_hash($password, PASSWORD_DEFAULT);			
							$account_entry = array('profid' => $profid, 'email_address' => $email, 'password' => $password_hash,'sec_question' => $sec_question,'sec_answer' => $sec_answer);
							$update = "UPDATE tbl_account set email_address=:email_address,password=:password,sec_question=:sec_question,sec_answer=:sec_answer where profile_id=:profid";

							$result = save($con,$account_entry,$update);

						}else{			
							$account_entry = array('profid' => $profid, 'email_address' => $email);
							$update = "UPDATE tbl_account set email_address=:email_address where profile_id=:profid";

							$result = save($con,$account_entry,$update);
						}
						echo $result;
				}

				
			}else{
				$entry = array('fn' => $fn,'mn' => $mn,'ln' => $ln,'gender' => $gender,'department' => $department,'position' => $position);
				$sql = "INSERT INTO tbl_profile(fn,mn,ln,gender,department,position) VALUES(:fn,:mn,:ln,:gender,:department,:position)";
				$last_id = GetLastId($con,$entry,$sql);


				$password_hash = password_hash($password, PASSWORD_DEFAULT);			
				$account_entry = array('profid' => $last_id, 'email_address' => $email, 'password' => $password_hash, 'sec_question' => $sec_question,'sec_answer' => $sec_answer);
				$insert = "INSERT INTO tbl_account(profile_id,email_address,password,sec_question,sec_answer) VALUES(:profid,:email_address,:password,:sec_question,:sec_answer)";

				echo save($con,$account_entry,$insert);
			}	
		}
	break;

	case 'delete_account':
		$type = $_POST['type'];

		$delete_val = null;
		if ($type == 1) {
			$delete_val = date('Y-m-d'); 
		}else{
			$delete_val = null;
		}

		$profid = $_POST['id'];
		$data = array('prof_id' => $profid, 'is_delete' => $delete_val);
		$sql = "UPDATE tbl_account set deleted = :is_delete where profile_id=:prof_id";

		echo save($con,$data,$sql);
	break;
	
	case 'delete_post_new':
			$type = $_POST['type'];

			$delete_val = 0;
			if ($type == 1) {
				$delete_val = 1; 
			}else{
				$delete_val = 0;
			}

			$profid = $_POST['id'];
			$data = array('post_id' => $profid, 'is_deleted' => $delete_val);
			$sql = "UPDATE tbl_announcement set is_deleted = :is_deleted where post_id=:post_id";

			echo save($con,$data,$sql);
	break;

	case 'delete_dean':
		$id = $_POST['dean_id'];
		$data = array('dean_id' => $id);
		$result = delete($con,'tbl_dean_list',$data);

		if ($result > 0) {
			echo 1;
		}


	break;

	case 'delete_reminder':
		$id = $_POST['rem_id'];
		$data = array('rem_id' => $id);
		$result = delete($con,'tbl_reminder',$data);

		if ($result > 0) {
			echo 1;
		}
	break;
	
	case 'change_password_account':
		$old_password = $_POST['old_password'];
		$confirm_password = $_POST['confirm_password'];

		$new_password = password_hash($confirm_password, PASSWORD_DEFAULT);

		$data_pass = array('password' => $new_password, 'id' => $id);
		$sql = "UPDATE tbl_account set password=:password where profile_id=:id";

		if (password_verify($old_password, $auth['password']) == true) {
			echo save($con,$data_pass,$sql);
		}else{
			echo 2;
		}

	break;

	case 'show_all_post_deleted':
		$data = array('myid' => $id);
		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_announcement b on a.profile_id=b.profile_id where b.profile_id=:myid and b.is_deleted = 1 order by post_id desc";
		$result = fetch_record($con,$data,$sql);

		
		while ($row  = $result->fetch()) {
			echo get_style_post_deleted($row);
		}
	break;

	case 'delete_post_backup':
		$postid = $_POST['id'];

		$file_path = '../webroot/uploads/';

		$data = array('id' => $postid);

		$sql1 = "SELECT * from tbl_announcement where post_id=:id";


		if (verify_record($con,$data,$sql1) > 0) {
			$result = fetch_record($con,$data,$sql1);
			$row = $result->fetch();

			$full_path = $file_path.$row['profile_id'].'/'.$row['is_file_path'];


			if (!empty($row['is_file_path'])) {
				unlink($full_path);
			}

			$sql = "DELETE from tbl_announcement where post_id=:id";
			echo save($con,$data,$sql);

			

		}
	break;

	case 'show_all_post_new':
		$type = $_POST['type'];

		$data = array();

		if ($auth['user_type'] == 1) {
			$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_announcement b on a.profile_id=b.profile_id where b.is_deleted = 0 order by b.post_id desc";
		}else{
			$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_announcement b on a.profile_id=b.profile_id where b.profile_id='$id' and b.is_deleted = 0 order by b.post_id desc";
		}

		$result = fetch_record($con,$data,$sql);
		$i = 0;
		while ($row = $result->fetch()) {
			$mname = (!empty($row['mn'])) ? $row['mn'][0] : '';
			$name = $row['ln'].', '.$row['fn'].' '.$mname.'.'; 
			$namess = $row['fn'].', '.$row['ln']; 
			$style =  (strtotime($row['date_interval']) >= strtotime(date('Y-m-d')))? 'opacity:1' : 'opacity:0.5';
			$active_stat = (strtotime($row['date_interval']) >= strtotime(date('Y-m-d')))? 'Active' : 'Inactive';

			$status = '';

			if (strtotime($row['date_interval']) >= strtotime(date('Y-m-d'))) {
				$status = 'active';
			}else if (strtotime($row['date_interval']) < strtotime(date('Y-m-d'))) {
				$status = 'inactive';
			}else{
				$status = 'all';
			}


			if ($type == $status) {
				$status = 'display:';
			}else if ($type == 'all') {
				$status = 'display:';
			}else{
				$status ='display:none';
			}

			$i++;
			$style_display = ''; 



			$file_path = '../webroot/uploads/';
			$full_path = $file_path.$row['profile_id'].'/'.$row['is_file_path'];


			$content = (!empty($row['content']))? $row['content'] : '~~~';

			$data_con = explode('~', $content);
			$why = $data_con[1];
			$where = $data_con[2];
			$when = $data_con[3];

			$file_ext = explode('.', $row['is_file_path']);
			$extention = strtolower(end($file_ext));
			$dt_edited = (!empty($row['date_edited'])) ? date('Y/m/d', strtotime($row['date_edited'])) : '';
			?>
			<tr style="<?php echo $status ?> cursor: pointer;">
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>" class="text-center"><?php echo $i; ?>
				 	
				 </td>
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>" class="text-capitalize"><?php echo $name; ?>
				 	
				 </td>
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>"><?php echo $row['title'] ?>
				 	
				 </td>
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>"><?php echo date('Y/m/d', strtotime($row['date_posted'])) ?>
				 	
				 </td>
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>"><?php echo $dt_edited ?>
				 	
				 </td>
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>"><?php echo $row['type'] ?>
				 	
				 </td>
				<td onclick="show_view('<?php echo $why ?>','<?php echo $where ?>','<?php echo $when ?>','<?php echo $extention ?>','<?php echo $row['is_file_path'] ?>','<?php echo $namess ?>','<?php echo $row['date_posted'] ?>','<?php echo $row['type'] ?>')" 
				 style="<?php echo $style ?>" class="text-center"><?php echo $active_stat ?></td>
				<td class="text-center">
					<button class="btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
					<div class="dropdown-menu">
					<button class="dropdown-item"><i class="fa fa-eye text-info"></i> View</button>
					
					<!-- <button class="dropdown-item" onclick="delete_post_new('<?php echo $row['post_id'] ?>','<?php echo $row['type'] ?>',0);"><i class="fa fa-check text-success"></i> Activate</button> -->
					

					<?php if ($row['type'] == "Schedule"): ?>
						<button class="dropdown-item" onclick="edit_post('<?php echo $row['post_id'] ?>','<?php echo $row['title'] ?>','<?php echo $row['content'] ?>','<?php echo $row['type'] ?>','<?php echo $row['is_file_path'] ?>','<?php echo $full_path ?>','<?php echo $row['date_interval'] ?>'); $('#modal_posting').modal('show');"><i class="fa fa-edit text-success"></i> Edit</button>
					<?php endif ?>


					<?php if ($row['type'] == "Announcement"): ?>
						<button class="dropdown-item" onclick="edit_post('<?php echo $row['post_id'] ?>','<?php echo $row['title'] ?>','<?php echo $row['content'] ?>','<?php echo $row['type'] ?>','<?php echo $row['is_file_path'] ?>','<?php echo $full_path ?>','<?php echo $row['date_interval'] ?>'); $('#modal_posting').modal('show');"><i class="fa fa-edit text-success"></i> Edit</button>
					<?php endif ?>
					

					<?php if ($row['type'] == "Activity"): ?>
						<button class="dropdown-item" onclick="edit_post('<?php echo $row['post_id'] ?>','<?php echo $row['title'] ?>','<?php echo $row['content'] ?>','<?php echo $row['type'] ?>','<?php echo $row['is_file_path'] ?>','<?php echo $full_path ?>','<?php echo $row['date_interval'] ?>'); $('#modal_posting').modal('show');"><i class="fa fa-edit text-success"></i> Edit</button>
					<?php endif ?>


					<button class="dropdown-item" onclick="delete_post_new('<?php echo $row['post_id'] ?>','<?php echo $row['type'] ?>',1);"><i class="fa fa-times text-danger"></i> Delete</button>
					</div>
					
				</td>
			</tr>
			<?php
		}
	break;

	//for testing purposes only
	case 'backup_database':
		$date_today = 'db_ama_'.date('Y-m-d');
		$file_path = "../webroot/database/".$date_today.'.sql';

		$res = exec('C:/xampp/mysql/bin/mysqldump -u root db_ama --routines >'.$file_path);
		
		echo 1;

	break;

	case 'recover_post':
		$postid = $_POST['id'];

		$data = array('id' => $postid, 'is_deleted' => 0);
		$sql = "UPDATE tbl_announcement set is_deleted=:is_deleted where post_id=:id";

		echo save($con,$data,$sql);
	break;
	
	// include("fileupload_helper.php");
	case 'dismiss_file':
		$id = $_SESSION['profile_id'];
		$path = $_POST['path'];
		if (unlink('../uploads/'.$id.'/'.$id.'_'.$path)) {
			echo $path;
		}
	break;

	default:
		die('Action '.$action.' is not exist !');
	break;
}

 ?>