<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AMA Bulletin Board</title>
   <link rel="shortcut icon" href="webroot/img/logo.png">
  <!-- Font Awesome Icons -->
  <link href="webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="webroot/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="webroot/css/creative.min.css" rel="stylesheet">
  <link href="assets/css/animate.css" rel="stylesheet">

<style type="text/css">
    html,body{
       font-family: calibri;
      background-color: #f1f1f1;
      width: 100%;
      height: 100%;
    }
    /* width */
    ::-webkit-scrollbar {
      width: 1px;
      border-radius:5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
      border-radius:5px;
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
      border-radius:5px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    }
    .post-wall-rem{
    /*  overflow: hidden;
      overflow-x: hidden;
      height: 110px;*/
      /*max-height: 110px;*/
      font-family: calibri;
    }
    .post_bg{
      background-size: contain;
      background-repeat: no-repeat;
      /*background-attachment: fixed;*/
      background-position: center;
    }
    #myVideo {
    /*position: fixed;*/
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
  }

  .post-wall{
      overflow: auto;
      overflow-x: hidden;
      /*height: 450px;*/
      /*max-height: 450px;*/
      font-family: calibri;
      padding: 20px;
    }

@media screen and (min-width: 1366px) {
  .post-wall{
      overflow: auto;
      overflow-x: hidden;
      /*min-height: 450px;*/
      height:550px;

      /*max-height: 450px;*/
      font-family: calibri;
      padding: 20px;
    }
}

/* On screens that are 600px or less, set the background color to olive */
@media screen and (min-width: 1678px) {
  .post-wall{
      overflow: auto;
      overflow-x: hidden;
      /*min-height: 450px;*/
      height: 850px;
      /*max-height: 450px;*/
      font-family: calibri;
      padding: 20px;
    }
}

  /* Add some content at the bottom of the video/page */
  .contents {
    position: fixed;
    bottom: 0;
    background: rgba(0, 0, 0);
    color: #f1f1f1;
    width: 100%;
    padding: 10px;
    height: 35px;
  }

  /* Style the button used to pause/play the video */
  #myBtn {
    width: 100px;
    font-size: 18px;
    background: #000;
    color: #fff;
    cursor: pointer;
    opacity: 0.1;
    transition: opacity 0.5s;
  }

  #myBtn:hover {
    /*background: #ddd;*/
    /*color: black;*/
    opacity: 1;

  }
  .opacity-off{
    background: rgba(0,0,0,0.0);
  }
  .opacity-on{
    background: rgba(0,0,0,0.2);
  }
  .bg{
    background-image: url(webroot/img/asd.png);
    background-position: center;
    background-size: cover;
    background-attachment: fixed;
  }
    </style>

    <style type="text/css">
      /* The side navigation menu */
  .sidebar {
    margin: 0;
    padding: 0;
    width: 200px;
    background-color: rgba(0,0,0,0.5);
    position: fixed;
    height: 100%;
    overflow: auto;
  }

  /* Sidebar links */
  .sidebar a {
    display: block;
    color: black;
    padding: 16px;
    text-decoration: none;
    border: solid 1px rgba(0,0,0,0.1);
  }

  /* Active/current link */
  .sidebar a.active {
    /*background-color: #4CAF50;*/
    color: white;
    transition: background-color 0.4s;
  }

  /* Links on mouse-over */
  .sidebar a:hover:not(.active) {
    background-color: rgba(0,0,0,0.7);
    color: white;
  }

  .sidebar a:hover{
    background-color: #555;
    color: white;
  }

  /* Page content. The value of the margin-left property should match the value of the sidebar's width property */
  div.content {
    margin-left: 200px;
    padding: 1px 16px;
    /*height: 1000px;*/
  }

  /* On screens that are less than 700px wide, make the sidebar into a topbar */
  @media screen and (max-width: 700px) {
    .sidebar {
      width: 100%;
      height: auto;
      position: relative;
    }
    .sidebar a {float: left;}
    div.content {margin-left: 0;}
  }

  /* On screens that are less than 400px, display the bar vertically, instead of horizontally */
  @media screen and (max-width: 400px) {
    .sidebar a {
      text-align: center;
      float: none;
    }
  }
  .padding-1{
    padding: 0px !important;
  }
</style>
<style type="text/css">
  .img-text-container {
    position: relative;
    text-align: center;
    color: white;
  }

  /* Bottom left text */
  .bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;
  }

  /* Top left text */
  .top-left {
    position: absolute;
    top: 8px;
    left: 16px;
  }

  /* Top right text */
  .top-right {
    position: absolute;
    top: 8px;
    right: 16px;
  }

  /* Bottom right text */
  .bottom-right {
    position: absolute;
    bottom: 8px;
    right: 16px;
  }

  /* Centered text */
  .centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .hover-more{
    cursor: pointer;
    opacity: 0.8;
    transition: opacity 0.4s;
  }
  .hover-more:hover{
    opacity: 0.4;
  }
</style>
</head>



<body id="page-top" class="bg" onload=" show_all_trigger(); show_all_announcement(); show_all_marquee();" style="background-color: #f1f1f1; overflow: hidden;">

  <!-- Navigation -->
  <!-- onclick="flip_board(); change_page();" -->
  <nav class="navbar navbar-expand navbar-dark navbar-light  m-0 p-0 fixed-top" id="nav_id">
    <div class="container-fluid">
      <button class="btn btn-dark btn-sm" style="display: none; margin: 5px;" data-toggle="dropdown"  id="btn_flip"><i id="btnicon" class="fa fa-bars animated flipInY"></i></button>
      <!-- timer -->
   <!--    <div class="dropdown-menu">
          <a href="#" onclick="flip_board(); change_page(); page_content(1);" class="dropdown-item">Announcement</a>
           <a href="#" onclick="flip_board(); change_page(); page_content(2);" class="dropdown-item">Schedule</a>
          <a href="#" onclick="flip_board(); change_page(); page_content(3);" class="dropdown-item">Activity</a>
         
      </div> -->
      
       <div class="timer text-light h3 w3-round" style="display: none;">
          <div id="counter">
              <div class="values"></div>
          </div>
        </div>
        <!-- timer -->
    </div>
  </nav>
<!-- 
  <div class="sidebar" id="sidebar" style="z-index: 10000;">
    <div class="text-center" style="padding:30px; background-color: rgba(255,255,255,0.1);">
      <img src="webroot/img/logo2.png" class="img-fluid">
    </div>
    <a class="active" href="#home" onclick="change_page(); home_dash();">Home</a>
    <a class="active" href="#home" onclick="flip_board(); change_page(); page_content(1);">Announcement</a>
    <a href="#contact" class="active" onclick="flip_board(); change_page(); page_content(3);">Program</a>
    <a href="#news" class="active" onclick="flip_board(); change_page(); page_content(2);">Schedule</a>
     --><!-- <a href="#about" class="active" onclick="flip_board(); change_page(); page_content(1);">About</a> -->
  <!-- </div> -->

  <div class="sidebar" id="sidebar" style="z-index: 10000;">
    <div class="text-center" style="padding:30px; background-color: rgba(255,255,255,0.1);">
      <img src="webroot/img/logo2.png" class="img-fluid">
    </div>
    <a class="active" href="#home" onclick="change_page(); home_dash();"> Home</a>
    <a class="active" href="#home" onclick="flip_board(); change_page(); page_content(1);"> Academic Calendar</a>
    <!-- <a class="active" href="#home" onclick="flip_board(); change_page(); page_content(1);">Announcement</a> -->
    <a href="#contact" class="active" onclick="flip_board(); change_page(); page_content(3);"> Program</a>
    <a href="#contact" class="active" onclick="flip_board(); change_page(); page_content(5);"> Reminder</a>
    <a href="#news" class="active" onclick="flip_board(); change_page(); page_content(2);"> Schedule</a>
    <a href="#news" class="active" onclick="flip_board(); change_page(); page_content(4);  show_deans_board();"> Dean's Lister</a>
    <!-- <a href="#about" class="active" onclick="flip_board(); change_page(); page_content(1);">About</a> -->
  </div>


  <div style="margin-left:15%;" class="content" id="contents">
     <!-- Masthead -->
    <div class="container-fluid " id="container" style="margin-top: 10px;">
      <div id="br"><br><br></div>
      <div class="row text-center animated fadeIn" id="video_page">
          <div class="col-sm-8 mt-2 p-1">
                <div id="demo" class="carousel slide " data-ride="carousel" style="padding: 0px;">

                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                  </ul>


                  <div class="carousel-inner" style="padding: 0px;">

                    <div class="carousel-item active ">
                      <img src="webroot/img/portfolio/fullsize/1.jpg" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">
                    </div>
                    <div class="carousel-item">
                     <img src="webroot/img/portfolio/fullsize/2.jpg" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">
                    </div>
                    <div class="carousel-item">
                      <img src="webroot/img/portfolio/fullsize/3.jpg" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">
                    </div>
                  </div>

                  <a class="carousel-control-prev " href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next " href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>

                </div>
          </div>
          <div class="col-sm-4 p-1 mt-2">
            <div>
                 <video autoplay="" controls=""  loop="" muted="" class="img-fluid rounded">
                  <source src="ama.mp4" type="video/mp4">
                </video>
            </div>

            <div class="mt-1 card">
              <div id="demos" class="carousel slide " data-ride="carousel" style="padding: 0px;">

                <!--   <ul class="carousel-indicators">
                    <li data-target="#demos" data-slide-to="0" class="active"></li>
                    <li data-target="#demos" data-slide-to="1"></li>
                    <li data-target="#demos" data-slide-to="2"></li>
                  </ul> -->

                  <div class="carousel-inner" style="padding: 0px;">
                    <div  id="announcement_panel"></div>
                  </div>

                  <a class="carousel-control-prev " href="#demos" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next " href="#demos" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>

                </div>

            
            </div>
             
          </div>
      </div>
  </div>

  

  <div class="page-section animated fadeOut" id="services" style="display: none; padding-left:10px; padding-top: 1px; max-height:710px !important; height: 710px; width: 100% !important; max-width:100% !important; ">
    <div class="container-fluid">
      <div class="row"><!-- 
        <div class="col-lg-1">
         
        </div> -->
        <div class="col-lg-12">
         

          <div class="mt-1">
            <div class="bg-white p-3 rounded">
                <div class="Announcement">
                    <img src="webroot/img/sched.png" class="img-fluid mb-3"  width=50>
                    <span class="h5 mb-2">Academic Calendar</span>
                </div>
               
                <div class="Activity">
                  <img src="webroot/img/program.png" class="img-fluid mb-3"  width=50>
                  <span class="h5 mb-2">Program</span>
                </div>

                <div class="Reminder">
                  <img src="webroot/img/bell.png" class="img-fluid mb-3"  width=50>
                  <span class="h5 mb-2">Reminder</span>
                </div>

                <div class="Schedule">
                  <img src="webroot/img/calendar.png" class="img-fluid mb-3"  width=50>
                  <span class="h5 mb-2">Schedule</span>
                </div>

                 <div class="Deans">
                  <img src="webroot/img/act.png" class="img-fluid mb-3"  width=50>
                  <span class="h5 mb-2">Dean's Lister</span>
                </div>
                  


            </div>
            <div class="form-group mt-1 ">
              <div class="input-group mb-3 " id="search_panel_for_index">
                  <div class="input-group-prepend">
                  <button class="btn btn-danger" onclick="$('#date_range').val(''); show_all_announcement();" type="button"><i class="fa fa-times"></i></button>
                </div>
                 <input type="date" name="date_range" id="date_range" class="form-control" oninput="show_all_announcement();">
              </div>

              <div class="input-group mb-3 " id="search_panel_for_index_a">
                 <input type="text" name="filter" id="filter" class="form-control" placeholder="Search..." oninput="show_all_announcement();">
              </div>
             
            </div>
         
            <hr>
           <div class="post-wall p-0">
              <div id="announcement_board" class="Announcement"></div>

               <div id="schedule_board" class="Schedule"></div>

               <div id="activity_board" class="Activity"></div>

               <div id="deans_board" class="Deans"></div>

               <div id="reminder_board" class="Reminder"></div>

            <br><br>
            <br><br>
            <br><br>
            <br><br>
            </div>
            
          </div>
        </div>
         <!--  <div class="col-lg-3">
         
        </div> -->

        </div>
      </div>
    </div>
  </div>
  



   <div class="contents" id="marquee">
      <marquee>
        AMA COMPUTER COLLEGE NAVIGATIONAL BULLETIN BOARD
     <!--    <div id="announcement_panel">
        </div> -->
      </marquee>
         
      <!-- Use a button to pause/play the video with JavaScript -->
      <!-- <button id="myBtn" class="btn btn-dark btn-sm" onclick="myFunction()">Pause</button> -->
    </div>
 


  <!-- Bootstrap core JavaScript -->
  <script src="webroot/vendor/jquery/jquery.min.js"></script>
  <script src="webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="webroot/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="webroot/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="webroot/js/creative.min.js" ></script>
  <script src="webroot/js/easytimer.min.js" ></script>
  <script src="webroot/js/index_a.js" ></script>

</body>

</html>

 <div class="modal fade" role="dialog" id="modal_preview_files">
        <div class="modal-dialog modal-lg mt-0">
          <div class="modal-content">
            <div class="modal-header p-2 pr-3">
                <div class="modal-title" id="modal_preview_title">
                  Loading...
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p-0">
         


               <div id="demos_1" class="carousel slide p-3" data-ride="carousel" style="padding: 0px; background-color: rgba(0,0,0,0.9);">
                    <div class="carousel-inner" style="padding: 0px;">
                      <div  id="preview_panel"></div>
                    </div>

                    <a class="carousel-control-prev " href="#demos_1" data-slide="prev">
                      <span class="carousel-control-prev-icon "></span>
                    </a>
                    <a class="carousel-control-next " href="#demos_1" data-slide="next">
                      <span class="carousel-control-next-icon "></span>
                    </a>

                  </div>
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>

<!--  <script type="text/javascript">
   
   
    
  </script> -->

<script type="text/javascript">
  function page_content(i){
    if (i == 1) {
      $(".Announcement").show('fast');
      $(".Schedule").hide('fast');
      $(".Activity").hide('fast');
      $(".Deans").hide('fast');
      $(".Reminder").hide('fast');
      $("#search_panel_for_index").hide('fast');
    }else if (i == 2) {
      $(".Schedule").show();
      $(".Announcement").hide('fast');
      $(".Activity").hide('fast');
      $(".Deans").hide('fast');
      $(".Reminder").hide('fast');
            $("#search_panel_for_index").show('fast');
    }else if (i == 3) {
      $(".Activity").show();
      $(".Schedule").hide();
      $(".Announcement").hide('fast');
      $(".Deans").hide('fast');
      $(".Reminder").hide('fast');
            $("#search_panel_for_index").hide('fast');
    }else if (i == 4) {
      $(".Activity").hide();
      $(".Schedule").hide();
      $(".Announcement").hide('fast');
      $(".Deans").show('fast');
      $(".Reminder").hide('fast');
            $("#search_panel_for_index").hide('fast');
    }else if (i == 5) {
      $(".Activity").hide();
      $(".Schedule").hide();
      $(".Announcement").hide('fast');
      $(".Deans").hide('fast');
      $(".Reminder").show('fast');
      $("#search_panel_for_index").hide('fast');
    }
  }
</script>