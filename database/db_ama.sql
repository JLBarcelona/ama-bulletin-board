-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2019 at 03:09 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ama`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_account`
--

CREATE TABLE `tbl_account` (
  `account_id` int(255) NOT NULL,
  `profile_id` int(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `password` longtext CHARACTER SET utf8,
  `api_key` longtext CHARACTER SET utf8,
  `user_type` int(1) DEFAULT '2',
  `date_entry` date DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `is_forgot` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_account`
--

INSERT INTO `tbl_account` (`account_id`, `profile_id`, `email_address`, `password`, `api_key`, `user_type`, `date_entry`, `deleted`, `is_forgot`) VALUES
(1, 1, 'admin@admin.com', '$2a$10$NuHyJre2K1fomxWK.S5Z7OKc18JCWBQsWEevgL8W/5i6ZRcYDNcV6', '', 1, '2019-09-07', NULL, 0),
(2, 2, 'johnluisb14@gmail.com', '$2y$10$ZEqKKZHq/tE0s43teU2GjuCqy46XIiIQ/YKCT/1tUan88t5Pe7qne', NULL, 2, NULL, NULL, 0),
(3, 3, 'user@user.com', '$2a$10$NuHyJre2K1fomxWK.S5Z7OKc18JCWBQsWEevgL8W/5i6ZRcYDNcV6', NULL, 2, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_announcement`
--

CREATE TABLE `tbl_announcement` (
  `post_id` int(255) NOT NULL,
  `profile_id` int(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `type` varchar(255) DEFAULT NULL,
  `search_keyword` varchar(255) DEFAULT NULL,
  `is_file_path` varchar(255) DEFAULT NULL,
  `date_interval` date DEFAULT NULL,
  `date_posted` datetime(6) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_announcement`
--

INSERT INTO `tbl_announcement` (`post_id`, `profile_id`, `title`, `content`, `type`, `search_keyword`, `is_file_path`, `date_interval`, `date_posted`, `is_deleted`) VALUES
(26, 1, 'Prelim', 'SAmple content', 'Schedule', NULL, '5d74d1b6a19d67.37724394.jpg', NULL, '2019-09-08 12:02:49.000000', 1),
(27, 1, 'An Example of a Google Bar Chart', 'HEllo world!', 'Announcement', NULL, '', NULL, '2019-09-08 12:03:14.000000', 1),
(28, 2, 'An Example of a Google Bar Chart', 'testing1234', 'Announcement', NULL, '', NULL, '2019-09-08 12:04:05.000000', 1),
(29, 1, 'Highcharts Demo', 'HEllo', 'Activity', NULL, '', NULL, '2019-09-08 12:06:04.000000', 1),
(30, 1, 'An Example of a Google Bar Chart', 'Schedule for exam', 'Schedule', NULL, '5d75b7faa82ce2.94269462.jpg', NULL, '2019-09-09 04:25:00.000000', 1),
(31, 1, 'An Example of a Google Bar Chart', 'asdasdasd', 'Announcement', NULL, '', NULL, '2019-09-09 04:25:12.000000', 1),
(32, 1, 'Dual-Y Example', 'asdasdasd', 'Schedule', NULL, '5d75b83364a5d0.62409119.jpg', NULL, '2019-09-09 04:25:58.000000', 1),
(33, 1, 'Dual-Y Example', 'asdasd', 'Announcement', NULL, '', NULL, '2019-09-09 04:26:06.000000', 1),
(34, 1, 'Highcharts Demo', 'asdasdasd', 'Schedule', NULL, '5d75c5e519afd8.07666499.jpg', NULL, '2019-09-09 04:26:37.000000', 1),
(35, 1, 'Dual-Y Example', 'asdasd', 'Activity', NULL, '', NULL, '2019-09-09 04:27:41.000000', 1),
(36, 1, '111111111', '22222222222', 'Activity', NULL, '', NULL, '2019-09-09 04:27:47.000000', 1),
(37, 1, 'An Example of a Google Bar Chart', 'asdsad', 'Announcement', NULL, '', NULL, '2019-09-09 05:38:12.000000', 1),
(38, 3, 'An Example of a Google Bar Chart', 'asdasd', 'Announcement', NULL, '', NULL, '2019-09-09 05:39:26.000000', 0),
(39, 1, 'Dual-Y Example', 'Sample', 'Schedule', NULL, '5d75e137c7b9c6.40152388.jpg', NULL, '2019-09-09 07:20:57.000000', 0),
(40, 1, 'Dual-Y Example', 'asdasd', 'Schedule', NULL, '5d75e182935a26.11465428.jpg', NULL, '2019-09-09 07:22:12.000000', 0),
(41, 1, 'Dual-Y Example', 'dasdasd', 'Announcement', NULL, '', NULL, '2019-09-09 07:27:24.000000', 0),
(42, 1, 'Dual-Y Example', 'sample', 'Schedule', NULL, '5d7622ce158976.84098499.jpg', NULL, '2019-09-09 12:00:55.000000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profile`
--

CREATE TABLE `tbl_profile` (
  `profile_id` int(255) NOT NULL,
  `fn` varchar(255) DEFAULT NULL,
  `mn` varchar(255) DEFAULT NULL,
  `ln` varchar(255) DEFAULT NULL,
  `bdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT 'Faculty',
  `date_entry` date DEFAULT NULL,
  `deleted` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_profile`
--

INSERT INTO `tbl_profile` (`profile_id`, `fn`, `mn`, `ln`, `bdate`, `gender`, `address`, `position`, `date_entry`, `deleted`) VALUES
(1, 'John Luis', 's.', 'Barcelona', '2019-09-27', 'Male', 'Santiago City', 'Administrator', '2019-09-07', NULL),
(2, 'Bruno', 'Soriano', 'Mars', NULL, 'Male', NULL, 'Faculty', NULL, NULL),
(3, 'Jeff', 'S.', 'Guya', NULL, 'Male', NULL, 'Faculty', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_account`
--
ALTER TABLE `tbl_account`
  ADD PRIMARY KEY (`account_id`),
  ADD KEY `profile` (`profile_id`);

--
-- Indexes for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `posted` (`profile_id`);

--
-- Indexes for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  ADD PRIMARY KEY (`profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_account`
--
ALTER TABLE `tbl_account`
  MODIFY `account_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  MODIFY `post_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  MODIFY `profile_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_account`
--
ALTER TABLE `tbl_account`
  ADD CONSTRAINT `tbl_account_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE;

--
-- Constraints for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  ADD CONSTRAINT `posted` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
