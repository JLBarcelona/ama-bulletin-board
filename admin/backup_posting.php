<!DOCTYPE html>
<html>
    
    <?php include("header.php") ?>
  <body onload="show_wall(); set_show_others_wall_update(); sidebar_active('posting');  show_others_wall();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">Post Announcement</h2>
                  </div>
                  <div class="card-body">
                    <div id="form_submit">
                    <div class="row">
                      <input type="hidden" name="post_id" id="post_id">
                      <div class="form-group col-lg-6">
                        <label>Announcement Type</label>
                        <select class="form-control" id="type" oninput="post_types(this.value);">
                          <option selected="" disabled="" value="">Select announcement type</option>
                          <option>Announcement</option>
                          <option>Activity</option>
                          <option>Schedule</option>
                        </select>
                      </div>  

                      <div class="form-group col-lg-6">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Title">
                      </div>  
                    </div>
                     

                    <div class="form-group">
                      <label>Content</label>
                       <textarea class="form-control" id="content" placeholder="Write something..." style="height: 114px;"></textarea>
                    </div>

                    <div class="form-group">
                      <label>Post Expiration</label>
                       <input type="date"  class="form-control" id="exp_date">
                    </div>

                    <hr>
                     
                     <div class="form-group" id="is_file" style="display: none;">
                       <div class=" text-uppercase font-weight-bold small headings-font-family mb-2">Upload Schedule</div>
                   
                         <input type="file" name="file" id="file" hidden="">
                         <div class="input-group mb-3">
                          <div class="input-group-prepend"> <a href="#" class="btn btn-success" id="btn_upload" onclick="$('#file').click();"><i class="fa fa-image"></i> Choose Photo</a href="#"></div>
                          <input type="text" name="file_name" id="file_name" class="form-control" placeholder="Choose File..." readonly="">
                        </div>
                    

                   
                         <div class="row">
                           <div class="col-sm-3">
                               <div id="img_preview">
                                  <input type="hidden" name="fie_path" id="file_path" value="">
                                </div>
                           </div>
                      </div>
                    </div>

                    </div>
                    <div class="form-group text-right">
                      <button class="btn btn-secondary" style="display: none;" id="btn_cancel" onclick="cancel_edit_post();">Cancel</button>
                      <button class="btn btn-primary" onclick="post_wall();">Post</button>
                    </div>
                   
                  </div>
                </div>

                <div id="loader"></div>
                <div id="wall_data"></div>
              </div>


              <div class="col-lg-5 mb-4 mb-lg-0 pl-lg-0">
               <div id="other_wall_data"></div>
              </div>

            </div>
          </section>

        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>

<script type="text/javascript">
  function backUpDatabase(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=backup_database',
      cache:false,
      success:function(data){
        alert(data);
      }
    });
  }
</script>


<script type="text/javascript">
  function post_wall(){
    var type = $("#type");
    var title = $("#title");
    var content = $("#content");
    var file_path = $("#file_path");
    var file = $("#file");
    var post_id = $("#post_id");
    var exp_date = $("#exp_date");

    // alert($("#file_name").val());
    if (type.val() == "" || type.val() == null) {
      type.focus();
    }
    else if (title.val() == "") {
      title.focus();
    }
    else if (content.val() == "") {
      content.focus();
    }else if (exp_date.val() == "" || exp_date.val() == null) {
      exp_date.focus();
    }
    else if (type.val() == "Schedule" && file.val() == "" && $("#file_name").val() == "") {
      file_path.focus();
      swal("Error","Please upload photo!","error");
    }else{
      var mydata = 'action=save_announcement' + '&type=' + type.val() + '&title=' + title.val() + '&content=' + content.val() + '&file_path=' + file_path.val() + '&post_id=' + post_id.val() + '&exp_date=' + exp_date.val();
      
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
        },
        success:function(data){
        
          if (data.trim() == 1) {
            // swal("Success","Announcement has been posted!","success");
            clear_upload();
            if (type.val() == "Schedule") {
              $("#is_file").hide('fast');
              $("#btn_delete").click();
            }
            type.val(null);
            title.val('');
            content.val('');
            file_path.val('');
            exp_date.val('');
            $("#loader").html('');
            post_id.val('');
            show_all_post();
            $("#btn_cancel").hide('fast');
            cancel_edit_post();

          }else{
            alert(data);
          }
        }
      });

    }


  }
</script>