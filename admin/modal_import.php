<div class="modal fade" role="dialog" id="import_excel_file">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
           Import Excel
          </div>
            <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <form id="import_excel">
                 <input type="hidden" name="action" id="action" value="import_excel_files">
                 <input type="file" id="file_import" name="file_import" class="form-control p-1">
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-success" id="btn_import" onclick="$('#import_excel').submit();">Import</button>
        </div>
      </div>
    </div>
  </div>

<!-- <script type="text/javascript" src="../webroot/js/import.js"></script> -->

<script type="text/javascript">


   $("#import_excel").on('submit', function(e){
      e.preventDefault();
      // var user_type = $("#user_import_type").val();

      // mydata = 'action=import_excel_files&' + $("#import_excel").serialize() + '&' + new FormData(this);
      // mydata = 'action=login';

      $.ajax({
        type:"POST",
        url:url,
        data:new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend:function(){
          $("#btn_import").attr('disabled', true);
          swal("Loading","Please Wait...","info");

        },
        success:function(data){
          console.log(data);
          // alert(data.trim()[0]);
          if (data.trim()[0] == 1) {
              // show_student();
              // show_teacher();
            swal("Success","Record has been import successfully!","success");
            setTimeout(function(){
              location.reload();
            },500);

            $("#file_import").val('');
            $("#import_excel_file").modal('hide');
          }
          $("#btn_import").attr('disabled', false);
        }
      });
    });
</script>