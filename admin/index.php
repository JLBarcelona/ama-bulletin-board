<!DOCTYPE html>
<html>
  <?php include("header.php") ?>
  <body onload="sidebar_active('home'); show_all_post(); show_reminders()">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              

              <div class="col-lg-4 mb-2 mb-lg-0">
                <div class="card bg-success text-white">
                  <div class="card-header text-dark">
                    <h2 class="h6 text-uppercase mb-0">Total Users</h2>
                  </div>
                  <div class="card-body">
                    <h3><?php echo count_dashboard($con,'user') ?></h3>
                  </div>
                </div>
              </div>

               <div class="col-lg-4 mb-2 mb-lg-0">
                <div class="card bg-primary text-white">
                  <div class="card-header text-dark">
                    <h2 class="h6 text-uppercase mb-0">Total Post</h2>
                  </div>
                  <div class="card-body">
                    <h3><?php echo count_dashboard($con,'post') ?></h3>
                  </div>
                </div>
              </div>

               <div class="col-lg-4 mb-2 mb-lg-0 ">
                <div class="card bg-info text-white">
                  <div class="card-header text-dark">
                    <h2 class="h6 text-uppercase mb-0">Total Reminder <!--  <button class="btn btn-success float-right" data-toggle="modal" data-backdrop="static" data-target="#modal_reminder"><i class="fa fa-plus-circle"></i></button> --></h2>

                  </div>
                  <div class="card-body">
                    <h3><?php echo count_dashboard($con,'reminder') ?></h3>
                  </div>
                </div>
              </div>

               


           <!--    <div class="col-lg-5 mb-4 mb-lg-0 pl-lg-0">
               <div id="other_wall_data"></div>
              </div> -->

            </div>
          </section>



          <section class="">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                      <div class="row">
                        <div class="col-sm-6 mb-2">
                           <h2 class="h6 text-uppercase mb-0">
                      <i class="fa fa-chalkboard"></i> List of Post
                    </h2>
                        </div>
                          <div class="col-sm-4 text-right mb-2">
                            <select class="form-control" id="filter_post" oninput="show_all_post();">
                              <option value="all">All</option>
                              <option value="active">Active</option>
                              <option value="inactive">Inactive</option>
                            </select>
                          </div>
                          <div class="col-sm-2 text-right">
                                <button class=" btn btn-success" data-toggle="modal" data-target="#modal_posting" onclick='$("#is_file").show("fast"); content_fields($("#type").val());'>Add Post</button>
                          </div>
                        </div>
                  </div>
                  <div class="card-body">
                      
                    <table class="table card-text table-hover" id="tbl_all_post_new">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Subject</th>
                          <th>Date Created</th>
                          <th>Date Edited</th>
                          <th>Type</th>
                          <th>Status</th>
                          <th class="text-center">Option</th>
                        </tr>
                      </thead>
                      <tbody id="post_data">

                      </tbody>
                    </table>
                   
                  </div>
                </div>

              
              </div>
            </div>
          </section>



          <section class="">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">
                      <i class="fa fa-bullhorn"></i> Reminders
                     <button class="btn btn-success float-right" data-toggle="modal" data-backdrop="static" data-target="#reminder_modal"><i class="fa fa-plus-circle"></i> Add Reminders</button>
                    </h2>
                  </div>
                  <div class="card-body">
                    <table class="table card-text" id="tbl_reminders">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Reminder</th>
                          <th>Expiry</th>
                          <th>Status</th>
                          <th class="text-center">Option</th>
                        </tr>
                      </thead>
                      <tbody id="reminders_data">

                      </tbody>
                    </table>
                   
                  </div>
                </div>

              
              </div>
            </div>
          </section>

        </div>
    
      </div>
    </div>



     <div class="modal fade" role="dialog" id="reminder_modal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-title">
                Post Reminder
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                 <div class="row">
                    <div class="form-group col-sm-12">
                      <input type="hidden" id="rem_id" name="rem_id" placeholder="Reminder" class="form-control">
                    </div>
                    <div class="form-group col-sm-12">
                      <label>Reminder</label>
                     <textarea id="reminder_msg" name="reminder_msg" placeholder="Write reminder..." class="form-control"></textarea>
                    </div>
                    <div class="form-group col-sm-12">
                      <label>Date Expiry</label>
                      <input type="date" id="expiry" name="expiry" placeholder="Post Expiration" class="form-control">
                    </div>

                    <div class="col-sm-12 text-right">
                      <button class="btn btn-success" onclick="post_reminder()">Submit</button>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                
              </div>
            </div>
          </div>
        </div>





<!-- Javascript Function-->
<script>

  
function edit_reminder(rem_id,reminder_msg,expiry){
  $("#rem_id").val(rem_id);
  $("#reminder_msg").val(reminder_msg);
  $("#expiry").val(expiry);
  $("#reminder_modal").modal('show');
}

function post_reminder(){
  var rem_id = $("#rem_id");
  var reminder_msg = $("#reminder_msg");
  var expiry = $("#expiry");
  
  if(reminder_msg.val() == "" || reminder_msg.val() == null){
    reminder_msg.focus();
    swal('oops!','Reminder is required!','error');
  }else if(expiry.val() == "" || expiry.val() == null){
    expiry.focus();
    swal('oops!','Post Expiration is required!','error');
  }
  else{
    var mydata = 'action=add_reminder' + '&rem_id=' + rem_id.val() + '&reminder_msg=' + reminder_msg.val() + '&expiry=' + expiry.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
      },
      success:function(data){
        if(data.trim() == 1){
          // <!-- your success message or action here! -->
          swal("Success","Reminder has been posted!","success");
          rem_id.val('');
          reminder_msg.val('');
          expiry.val('');
          $("#reminder_modal").modal('hide');
          show_reminders();
        }else if (data.trim() == 2) {
          swal("Success","Reminder has been edited!","success");
          rem_id.val('');
          reminder_msg.val('');
          expiry.val('');
          $("#reminder_modal").modal('hide');
          show_reminders();
        }else{
          // <!-- your error message or action here! -->
          console.log(data.trim());
        }
      }
    });
  }
}


function show_reminders(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_reminder',
      cache:false,
      beforeSend:function(){
      },
      success:function(data){
        $("#reminders_data").html(data);
        $("#tbl_reminders").dataTable();
      }
    });
}

 function delete_reminder(id, name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+"?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_reminder' + '&rem_id=' + id,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            show_reminders();
             swal("Success","Reminder has been deleted!","success");
              setTimeout(function(){
                location.reload();
              },200);
          }else{
            console.log(data);
          }
        }
      });
    });
}
</script>


    <!-- JavaScript files-->
    <?php include("footer.php") ?>
    <?php include("modal.php") ?>
    
  </body>
</html>

