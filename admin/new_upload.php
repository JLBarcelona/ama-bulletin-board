<?php
session_start();
include('../function/config.php');
include('../function/helper.php');


$id = $_SESSION['profile_id'];

$file_location = '../uploads/'.$id;


if (!file_exists($file_location)) {
		mkdir($file_location);
	}
// A list of permitted file extensions
$allowed = array('png', 'jpg', 'gif','zip');

if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

	// if(!in_array(strtolower($extension), $allowed)){
	// 	echo '{"status":"error"}';
	// 	exit;
	// }

	$new_filename = $_FILES['upl']['name'];
	$destination = $file_location.'/'.$new_filename;

	if (!file_exists($destination)) {
			if(move_uploaded_file($_FILES['upl']['tmp_name'], $destination)){
				echo '{"status":"success"}';
				exit;
			}
	}else{
			echo '{"status":"error"}';
	}
}

// echo '{"status":"error"}';
exit;