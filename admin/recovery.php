<!DOCTYPE html>
<html>
    
    <?php include("header.php") ?>
  <body onload="show_wall_deleted();  sidebar_active('recovery');">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-body">
                    <h4><i class="fa fa-history"></i> Announcement backup and recovery</h4>
                  </div>
                </div>
                <div id="loader"></div>
                <div id="wall_data"></div>
              </div>

              <div class="col-lg-5 mb-4 mb-lg-0 pl-lg-0">
               <div id="other_wall_data"></div>
              </div>

            </div>
          </section>

        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>

<script type="text/javascript">
 
function recover_post(id){
  swal({
      title: "Are you sure?",
      text: "Do you want to recover this post ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=recover_post' + '&id=' + id,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
           show_wall_deleted();
             swal("Success","Post has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}

</script>