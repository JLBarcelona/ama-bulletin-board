<!DOCTYPE html>
<html>
    
  <?php include("header.php") ?>
  <body onload="sidebar_active('deans'); show_deans_lister();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-sm-4">
                        <h2 class="h6 text-uppercase mb-0">Dean's Lister</h2>
                      </div>
                      <div class="col-sm-8 text-right">
                        <button class=" btn btn-primary mr-1" data-toggle="modal" data-target="#import_excel_file" ><i class="fa fa-file-excel"></i> Import Dean's Lister</button>
                        <button class=" btn btn-success mr-1" data-toggle="modal" data-target="#add_dean"><i class="fa fa-user-plus"></i> Dean's Lister</button>
                        <!-- <button class=" btn btn-primary">Restore</button> -->
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <table class="table table-bordered" id="tbl_deans">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>USN</th>
                          <th>Name</th>
                          <th>Course</th>
                          <th>Year</th>
                          <th>Section</th>
                          <th>GPA</th>
                          <th width="5%" class="text-center" >Option</th>
                        </tr>
                      </thead>
                      <tbody id="deans_data">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>
    <?php include("modal_import.php") ?>

  </body>
</html>



 <div class="modal fade" role="dialog" id="add_dean">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Dean's Lister
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-sm-12">
                <input type="hidden" id="dean_id">
                <label>USN</label>
                <input type="text" id="usn" name="usn" placeholder="Usn" class="form-control">
              </div>
              <div class="form-group col-sm-6">
                <label>Firstname</label>
                <input type="text" id="f_name" name="f_name" placeholder="Firstname" class="form-control">
              </div>
              <div class="form-group col-sm-6">
                <label>Middlename</label>
                <input type="text" id="m_name" name="m_name" placeholder="Middlename" class="form-control">
              </div>
              <div class="form-group col-sm-12">
                <label>Lastname</label>
                <input type="text" id="l_name" name="l_name" placeholder="Lastname" class="form-control">
              </div>

               <div class="col-sm-4 form-group">
                      <label>Course</label>
                         <select  class="form-control" id="dean_course">
                           <option selected=""></option>
                           <option value="BSIT">BSIT</option>
                           <option value="BSCS">BSCS</option>
                           <option value="BSCpe">BSCpe</option>
                           <option value="BSECE">BSECE</option>
                         </select>
                     </div>


                     <div class="col-sm-4 form-group">
                      <label>Year</label>
                         <select  class="form-control" id="dean_year">
                           <option selected=""></option>
                           <option value="1">1</option>
                           <option value="2">2</option>
                           <option value="3">3</option>
                           <option value="4">4</option>
                         </select>
                     </div>

                     <div class="col-sm-4 form-group">
                      <label>Section</label>
                         <select  class="form-control" id="dean_section">
                           <option selected=""></option>
                            <option value="T1">T1</option>
                            <option value="T2">T2</option>
                            <option value="T3">T3</option>
                            <option value="C1">C1</option>
                            <option value="C2">C2</option>
                            <option value="C3">C3</option>
                            <option value="P1">P1</option>
                            <option value="P2">P2</option>
                            <option value="P3">P3</option>
                            <option value="E1">E1</option>
                            <option value="E2">E2</option>
                            <option value="E3">E3</option>
                            <option value="E4">E4</option>
                            <option value="O1">O1</option>
                            <option value="O2">O2</option>
                            <option value="O3">O3</option>
                            <option value="O4">O4</option>
                         </select>
                     </div>

              <div class="form-group col-sm-12">
                <label>GPA</label>
                <input type="number" id="average" name="average" placeholder="GPA" class="form-control">
              </div>

              <div class="col-sm-12 text-right">
                <button class="btn btn-success" onclick="save_deans_lister()">Submit</button>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>






<!-- Javascript Function-->
<script>
function save_deans_lister(){
  var f_name = $("#f_name");
  var m_name = $("#m_name");
  var l_name = $("#l_name");
  var dean_course = $("#dean_course");
  var dean_year = $("#dean_year");
  var dean_section = $("#dean_section");
  var average = $("#average");
  var usn = $("#usn");
  var dean_id = $("#dean_id");
   

  if(usn.val() == "" || usn.val() == null){
    usn.focus();
    swal('Oops!','Usn is required!','error');
  }else if(f_name.val() == "" || f_name.val() == null){
    f_name.focus();
    swal('Oops!','Firstname is required!','error');
  }else if(m_name.val() == "" || m_name.val() == null){
    m_name.focus();
    swal('Oops!','Middlename is required!','error');
  }else if(l_name.val() == "" || l_name.val() == null){
    l_name.focus();
    swal('Oops!','Lastname is required!','error');
  }else if(dean_course.val() == "" || dean_course.val() == null){
    dean_course.focus();
    swal('Oops!','Course is required!','error');
  }else if(dean_year.val() == "" || dean_year.val() == null){
    dean_year.focus();
    swal('Oops!','Year is required!','error');
  }else if(dean_section.val() == "" || dean_section.val() == null){
    dean_section.focus();
    swal('Oops!','Section is required!','error');
  }else if(average.val() == "" || average.val() == null){
    average.focus();
    swal('Oops!','Average is required!','error');
  }
  else{
    var mydata = 'action=save_deans_lister' + '&f_name=' + f_name.val() + '&m_name=' + m_name.val() + '&l_name=' + l_name.val() + '&dean_course=' + dean_course.val() + '&dean_year=' + dean_year.val() + '&dean_section=' + dean_section.val() + '&average=' + average.val() + '&usn=' + usn.val() + '&dean_id=' + dean_id.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(data){
        if (data.trim() == 404) {
          swal("Oops!","Student Id is already exist!","error");
        }else if(data.trim() == 1){
          //<!-- your success message or action here! -->
          show_deans_lister();
          swal("Success","Student has been save","success");
          usn.val('');
          f_name.val('');
          m_name.val('');
          l_name.val('');
          dean_course.val('');
          dean_year.val('');
          dean_section.val('');
          average.val('');
          dean_id.val('');
          $("#add_dean").modal('hide');

        }else if (data.trim() == 2) {
           show_deans_lister();
            swal("Success","Student has been edited","success");
            usn.val('');
            f_name.val('');
            m_name.val('');
            l_name.val('');
            dean_course.val('');
            dean_year.val('');
            dean_section.val('');
            average.val('');
            dean_id.val('');
            $("#add_dean").modal('hide');

        }else{
          //<!-- your error message or action here! -->
          console.log(data.trim());
        }
      }
    });
  }
}


function edit_dean(f_name,m_name,l_name,dean_course,dean_year,dean_section,average,usn,dean_id){
 $("#dean_id").val(dean_id);
 $("#f_name").val(f_name);
 $("#m_name").val(m_name);
 $("#l_name").val(l_name);
 $("#dean_course").val(dean_course);
 $("#dean_year").val(dean_year);
 $("#dean_section").val(dean_section);
 $("#average").val(average);
 $("#usn").val(usn);
 $("#add_dean").modal('show');
}

function show_deans_lister(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_dean_lister',
      cache:false,
      beforeSend:function(){
      },
      success:function(data){
        $("#deans_data").html(data);
        $("#tbl_deans").dataTable();
      }
    });
}


  function delete_dean_list(id, name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+"?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_dean' + '&dean_id=' + id,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
            show_deans_lister();
             swal("Success",name+" has been deleted!","success");
              setTimeout(function(){
                location.reload();
              },200);
          }else{
            console.log(data);
          }
        }
      });
    });
}

</script>
