<script src="../webroot/admin/vendor/jquery/jquery.min.js"></script>
<script src="../webroot/admin/vendor/popper.js/umd/popper.min.js"> </script>
<script src="../webroot/admin/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="../webroot/admin/vendor/chart.js/Chart.min.js"></script>
<script src="../webroot/js/admin.js"></script>
<script src="../webroot/js/tools.js"></script>
<script src="../assets/js/sweetalert.min.js"></script>


<script src="../webroot/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="../webroot/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="../webroot/upload_plugin/js/jquery.knob.js"></script>

<!-- jQuery File Upload Dependencies -->
<script src="../webroot/upload_plugin/js/jquery.ui.widget.js"></script>
<script src="../webroot/upload_plugin/js/jquery.iframe-transport.js"></script>
<script src="../webroot/upload_plugin/js/jquery.fileupload.js"></script>

<!-- Our main JS file -->
<script src="../webroot/upload_plugin/js/script.js"></script>
<script src="../webroot/upload_plugin/js/myjs.js"></script>