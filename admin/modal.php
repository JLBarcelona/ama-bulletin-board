 <div class="modal fade" role="dialog" id="modal_posting">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
              <h2 class="h6 text-uppercase mb-0">Post Announcement</h2>
            </div>
              <button class="close" data-dismiss="modal" onclick="cancel_edit_post();">&times;</button>
          </div>
          <div class="modal-body">
            <div id="form_submit">
              <div class="row">
                <input type="hidden" name="post_id" id="post_id">
                <div class="form-group col-lg-6">
                  <label>Announcement Type</label>
                  <!-- oninput="post_types(this.value);" -->
                  <select class="form-control" id="type" oninput="content_fields($(this).val());">
                    <!-- <option selected="" disabled="" value="">Select announcement type</option> -->
                    <option value="Announcement">Academic Calendar</option>
                    <option value="Activity">Program</option>
                    <option>Schedule</option>
                  </select>
                </div>  

                <div class="form-group col-lg-6">
                  <label>Title</label>
                  <input type="text" name="title" id="title" class="form-control" placeholder="Title">
                </div>  
              </div>
               
              <div class="row">
                 <div class="form-group col-sm-12 content_feild_label">
                  <label>Why</label>
                  <input type="text" name="why" id="why" oninput="content_post();" class="form-control" placeholder="Why">
                </div>
                <div class="form-group col-sm-6 content_feild_label">
                  <label>Where</label>
                  <input type="text" name="where" id="where" oninput="content_post();" class="form-control" placeholder="Where">
                </div>
                <div class="form-group col-sm-6 content_feild_label">
                  <label>When</label>
                  <input type="text" name="when" id="when" oninput="content_post();" class="form-control" placeholder="When">
                </div>
               
              </div>

              <div class="form-group">
                <!-- <label>Content</label> -->
                 <textarea class="form-control" hidden="" id="content" placeholder="Write something..." style="height: 114px;"></textarea>
              </div>

              <div class="form-group">
                <label>Post Expiration</label>
                 <input type="date"  class="form-control" id="exp_date">
              </div>

              <hr>
               
               <div class="form-group" id="is_file">
                 <div class=" text-uppercase font-weight-bold small headings-font-family mb-2">Upload File</div>

                   <!-- <input type="file" name="file" id="file" hidden="">
                   <div class="input-group mb-3">
                    <div class="input-group-prepend"> <a href="#" class="btn btn-success" id="btn_upload" onclick="$('#file').click();"> Choose </a href="#"></div>
                    <input type="text" name="file_name" id="file_name" class="form-control" placeholder="Choose File..." readonly="">
                  </div> -->

                  <form id="upload" method="post" action="new_upload.php" enctype="multipart/form-data">
                    <div id="drop">
                      Drop Here
                      <br>
                      <a>Browse</a>
                      <input type="file" name="upl" multiple />
                    </div>

                    <ul>
                      <!-- The file uploads will be shown here -->
                    </ul>

                  </form>


                   <div class="row">
                     <div class="col-sm-3">
                         <!-- <div id="img_preview"> -->
                            <textarea hidden="" name="fie_path" id="file_path"></textarea>
                          <!-- </div> -->
                     </div>
                </div>
              </div>

              </div>
              <div class="form-group text-right">
                <button class="btn btn-secondary" style="display: none;" id="btn_cancel" onclick="cancel_edit_post();">Cancel</button>
                <!-- <button class="btn btn-dark" onclick="show_array();"></button> -->
                <button class="btn btn-primary" onclick="post_wall();">Post</button>
              </div>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

     <div class="modal fade text-capitalize" role="dialog" id="modal_show">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-title" id="title_s">
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <span id="why_s"></span><br>
                <span id="where_s"></span><br>
                <span id="when_s"></span>
           <!--      <span id="extention"></span>
                <span id="filename"></span> -->

                <hr>
                 <div class="row">
                  <div class="col-sm-6 text-left">
                      <div id="posted_by" class="pull-left"></div>
                  </div>
                  <div class="col-sm-6 text-right">
                     <div id="posted_on" class="pull-right"></div>
                  </div>
                </div>
              </div>
           
            </div>
          </div>
        </div>

    <script type="text/javascript">
      var file_input_val = [];

    

      function show_array(){
        // console.log(file_input_val);
        var new_data = file_input_val.join("~");
        // console.log(new_data);
        $("#file_path").val(new_data);
      }

      function show_all_input_value(array,key){
        var new_data = '';

        for (var i = 0; i < array.length ; i++) {
           if (key === array[i]) {
            array.splice(i, 1);
            // console.log(array[i]);
            // console.log(key);
           }
        }

        // console.log(array);
  
      }


      function content_fields(vals){
        if (vals == 'Schedule') {
          disable_fields_content('');
        }else{
          disable_fields_content('none');
        }
      }

      function disable_fields_content(display){
        $(".content_feild_label").attr('style', 'display:'+display);
      }



      function content_post(){
        var where = $("#where");
        var when = $("#when");
        var why = $("#why");
        var msg = '';
        var where_content = (where.val() != "") ? "~"+ where.val() : '~';
        var when_content = (when.val() != "") ? "~"+ when.val() : '~';
        var why_content =  (why.val() != "") ? "~"+ why.val() : '~';

        msg += why_content;
        msg += where_content;
        msg += when_content;

        // console.log(msg);

        $("#content").val(msg);

      }


      function fetch_content(){
          var rr = [];
          var where = $("#where");
          var when = $("#when");
          var why = $("#why");
          var val = $("#content").val();
          var result = val.split("~");
            rr = result;

          where.val(rr[1]);
          when.val(rr[2]);
          why.val(rr[3]);

          // console.log(rr);


      }


    function post_wall(){
    var type = $("#type");
    var title = $("#title");
    var content = $("#content");
    var file_path = $("#file_path");
    var file = $("#file");
    var post_id = $("#post_id");
    var exp_date = $("#exp_date");

    // alert($("#file_name").val());
    if (type.val() == "" || type.val() == null) {
      type.focus();
    }
    else if (title.val() == "") {
      title.focus();
    }
    else if (exp_date.val() == "" || exp_date.val() == null) {
      exp_date.focus();
    }
    else{
      var mydata = 'action=save_announcement' + '&type=' + type.val() + '&title=' + title.val() + '&content=' + content.val() + '&file_path=' + file_path.val() + '&post_id=' + post_id.val() + '&exp_date=' + exp_date.val();
      
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
        },
        success:function(data){
        
          if (data.trim() == 1) {
            // swal("Success","Announcement has been posted!","success");
            clear_upload();
            $("#is_file").hide('fast');
            $("#btn_delete").click();
            type.val('Announcement');
            title.val('');
            content.val('');
            file_path.val('');
            exp_date.val('');
            $("#loader").html('');
            post_id.val('');
            show_all_post();
            $("#btn_cancel").hide('fast');
            cancel_edit_post();
            location.reload();

          }else{
            alert(data);
          }
        }
      });

    }


  }


  function delete_post_new(id, name, type){
    var message = '';

    if (type == 1) {
      message ='delete';
    }else if (type == 0) {
      message= 'acivate';
    }

  swal({
      title: "Are you sure?",
      text: "Do you want to "+message+" "+name+"?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_post_new' + '&id=' + id + '&type=' +type,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             show_all_post();
             swal("Success","Post has been "+message+"d!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}
    </script>