<!DOCTYPE html>
<html>
  
    <?php include("header.php") ?>
  <style type="text/css">
    html,body{
       font-family: calibri;
      background-color: #f1f1f1;
      width: 100%;
      height: 100%;
    }
    /* width */
    ::-webkit-scrollbar {
      width: 1px;
      border-radius:5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
      border-radius:5px;
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
      border-radius:5px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    }
    .post-wall-rem{
    /*  overflow: hidden;
      overflow-x: hidden;
      height: 110px;*/
      /*max-height: 110px;*/
      font-family: calibri;
    }
    .post_bg{
      background-size: contain;
      background-repeat: no-repeat;
      /*background-attachment: fixed;*/
      background-position: center;
    }
  .post-wall{
      overflow: auto;
      overflow-x: hidden;
      /*height: 450px;*/
      /*max-height: 450px;*/
      font-family: calibri;
      padding: 20px;
    }

@media screen and (min-width: 1366px) {
  .post-wall{
      overflow: auto;
      overflow-x: hidden;
      /*min-height: 450px;*/
      height:550px;

      /*max-height: 450px;*/
      font-family: calibri;
      padding: 20px;
    }
}

/* On screens that are 600px or less, set the background color to olive */
@media screen and (min-width: 1678px) {
  .post-wall{
      overflow: auto;
      overflow-x: hidden;
      /*min-height: 450px;*/
      height: 850px;
      /*max-height: 450px;*/
      font-family: calibri;
      padding: 20px;
    }
}

  /* Add some content at the bottom of the video/page */
  .contents {
    position: fixed;
    bottom: 0;
    background: rgba(0, 0, 0);
    color: #f1f1f1;
    width: 100%;
    padding: 10px;
    height: 35px;
  }

  /* Style the button used to pause/play the video */
  #myBtn {
    width: 100px;
    font-size: 18px;
    background: #000;
    color: #fff;
    cursor: pointer;
    opacity: 0.1;
    transition: opacity 0.5s;
  }

  #myBtn:hover {
    /*background: #ddd;*/
    /*color: black;*/
    opacity: 1;

  }
  .opacity-off{
    background: rgba(0,0,0,0.0);
  }
  .opacity-on{
    background: rgba(0,0,0,0.2);
  }
  .bg{
    background-image: url(../webroot/img/asd.png);
    background-position: center;
    background-size: cover;
    background-attachment: fixed;
  }

  .img-text-container {
    position: relative;
    text-align: center;
    color: white;
  }

  /* Bottom left text */
  .bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;
  }

  /* Top left text */
  .top-left {
    position: absolute;
    top: 8px;
    left: 16px;
  }

  /* Top right text */
  .top-right {
    position: absolute;
    top: 8px;
    right: 16px;
  }

  /* Bottom right text */
  .bottom-right {
    position: absolute;
    bottom: 8px;
    right: 16px;
  }

  /* Centered text */
  .centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .hover-more{
    cursor: pointer;
    opacity: 0.8;
    transition: opacity 0.4s;
  }
  .hover-more:hover{
    opacity: 0.4;
  }
    </style>
</head>
  <body onload="sidebar_active('deans'); deans_list();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
               <div class="card">
                  <div class="card-header">
                      <div class="row">
                        <div class="col-sm-6 mb-2">
                           <h2 class="h6 text-uppercase mb-0">
                      <i class="fa fa-chalkboard"></i> List of Post
                    </h2>
                        </div>
                          <div class="col-sm-12 text-right">
                              <button class=" btn btn-success" data-toggle="modal" data-target="#modal_posting" onclick='$("#is_file").show("fast"); content_fields($("#type").val());'>Add Post</button>
                          </div>
                        </div>
                  </div>
                  <div class="card-body">
                   <div id="post_data"></div>
                   
                  </div>
                </div>
              </div>

            </div>
          </section>

        </div>
    
      </div>
    </div>
  
   <div class="modal fade" role="dialog" id="modal_preview_files">
        <div class="modal-dialog modal-lg mt-0">
          <div class="modal-content">
            <div class="modal-header p-2 pr-3">
                <div class="modal-title" id="modal_preview_title">
                  Loading...
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p-0">
         


               <div id="demos_1" class="carousel slide p-3" data-ride="carousel" style="padding: 0px; background-color: rgba(0,0,0,0.9);">
                    <div class="carousel-inner" style="padding: 0px;">
                      <div  id="preview_panel"></div>
                    </div>

                    <a class="carousel-control-prev " href="#demos_1" data-slide="prev">
                      <span class="carousel-control-prev-icon "></span>
                    </a>
                    <a class="carousel-control-next " href="#demos_1" data-slide="next">
                      <span class="carousel-control-next-icon "></span>
                    </a>

                  </div>
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>


    
    <!-- JavaScript files-->
    <?php include("modal_dean.php") ?>
    <?php include("footer.php") ?>
  </body>
</html>

<script>
  function show_previews(title,arrays,file_path){
    $("#modal_preview_title").text(title);
    // $("#preview_panel").html();
    // alert(file_path);
    $("#preview_panel").html('<div  class="text-center p-4"><img src="../webroot/img/load.gif" width="50"></div>');
    var data = '';
    var arr = arrays.split("~");

    for (var i = 0; i < arr.length; i++) {
      // arr[i];
      // console.log(arr[i]);
      var filename = arr[i];

       // get file extension 
      var extensions = filename.split('.').pop();
      
      var is_active = (i == 0)? 'active' : '';
      if(extensions == 'pdf' || extensions == 'png' || extensions == 'jpg' || extensions == 'jpeg' || extensions == 'PNG' || extensions == 'JPG' || extensions == 'JPEG' || extensions == 'GIF' || extensions == 'gif'){
         data += ' <div class="carousel-item '+is_active+'">';
           // data += '<img src="'+file_path+filename+'" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">';
          data += '<div class="text-center"><img src="../webroot/img/img.png" class="  post_bg" style="width: 80%; height:80%; background-image: url('+file_path+filename+');"></div>';
           data += ' </div>';
      }else{
           data += ' <div class="carousel-item '+is_active+'">';
           // data += '<img src="'+file_path+filename+'" alt="Los Angeles" class="animated fadeIn img-fluid rounded" style="width: 100%;">';
           data += '<div class="text-center"><img src="../webroot/img/img.png" class="  post_bg" style="width: 80%; height:80%; background-image: url(../webroot/img/files.png);"></div>';

           data += ' <div class="text-center"><a class="btn btn-block btn-success btn-block" href="'+file_path+filename+'">Download '+filename+'</a></div>';


           data += ' </div>';
       
      }

      setTimeout(function(){
         $("#preview_panel").html(data);
      },1500);
    }

    console.log(data);

    $("#modal_preview_files").modal('show');
   }
</script>


<script>
  function deans_list(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_deans_post',
      cache:false,
      success:function(data){
        $("#post_data").html(data);
      }
    });
  }

 
</script>  