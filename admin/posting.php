<!DOCTYPE html>
<html>
    <style type="text/css">
      .dataTables_filter{
        display: none;
      }
      .dataTables_length{
        display: none;
      }
    </style>
    <?php include("header.php") ?>
       
  <body onload="sidebar_active('posting'); show_all_post();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
               <div class="card">
                  <div class="card-header">
                      <div class="row">
                        <div class="col-sm-6 mb-2">
                           <h2 class="h6 text-uppercase mb-0">
                      <i class="fa fa-chalkboard"></i> List of Post
                    </h2>
                        </div>
                          <div class="col-sm-4 text-right mb-2">
                            <select class="form-control" id="filter_post" oninput="show_all_post();">
                              <option value="all">All</option>
                              <option value="active">Active</option>
                              <option value="inactive">Inactive</option>
                            </select>
                          </div>
                          <div class="col-sm-1 text-right">
                              <button class=" btn btn-success" data-toggle="modal" data-target="#modal_posting" onclick='$("#is_file").show("fast"); content_fields($("#type").val());'>Add Post</button>
                            
                          </div>
                        </div>
                  </div>
                  <div class="card-body">
                      
                    <table class="table card-text table-hover" id="tbl_all_post_new">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Subject</th>
                          <th>Date Created</th>
                          <th>Date Edited</th>
                          <th>Type</th>
                          <th>Status</th>
                          <th class="text-center">Option</th>
                        </tr>
                      </thead>
                      <tbody id="post_data">

                      </tbody>
                    </table>
                   
                  </div>
                </div>
              </div>

            </div>
          </section>

        </div>
    
      </div>
    </div>


    
    <!-- JavaScript files-->
    <?php include("modal.php") ?>
    <?php include("footer.php") ?>
  </body>
</html>
