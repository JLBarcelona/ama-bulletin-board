<!DOCTYPE html>
<html>
    
    <?php include("header.php") ?>
  <body onload="show_wall();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">Post Announcement</h2>
                  </div>
                  <div class="card-body">
                    <div id="form_submit">
                    <div class="row">
                      <div class="form-group col-lg-6">
                        <label>Announcement Type</label>
                        <select class="form-control" id="type" oninput="post_types(this.value);">
                          <option selected="" disabled="">Select announcement type</option>
                          <option>Announcement</option>
                          <option>Activity</option>
                          <option>Schedule</option>
                        </select>
                      </div>  

                      <div class="form-group col-lg-6">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Title">
                      </div>  
                    </div>
                     

                    <div class="form-group">
                      <label>Content</label>
                       <textarea class="form-control" id="content" placeholder="Write something..."></textarea>
                    </div>

                    <hr>
                     
                     <div class="form-group" id="is_file" style="display: none;">
                       <div class=" text-uppercase font-weight-bold small headings-font-family mb-2">Upload Schedule</div>
                   
                         <input type="file" name="file" id="file" hidden="">
                         <div class="input-group mb-3">
                          <div class="input-group-prepend"> <a href="#" class="btn btn-success" id="btn_upload" onclick="$('#file').click();"><i class="fa fa-image"></i> Choose Photo</a href="#"></div>
                          <input type="text" name="file_name" id="file_name" class="form-control" placeholder="Choose File..." readonly="">
                        </div>
                    

                   
                         <div class="row">
                           <div class="col-sm-3">
                               <div id="img_preview">
                                  <input type="hidden" name="fie_path" id="file_path" value="">
                                </div>
                           </div>
                      </div>
                    </div>

                    </div>
                    <div class="form-group text-right">
                      <button class="btn btn-primary" onclick="post_wall();">Post</button>
                    </div>
                   
                  </div>
                </div>

                <div id="loader"></div>
                <div id="wall_data"></div>
              </div>


              <div class="col-lg-5 mb-4 mb-lg-0 pl-lg-0">
                <div class="card mb-3">
                  <div class="card-body">
                    <div class="row align-items-center flex-row">
                      <div class="col-lg-5">
                        <h2 class="mb-0 d-flex align-items-center"><span>86.4</span><span class="dot bg-green d-inline-block ml-3"></span></h2><span class="text-muted text-uppercase small">Work hours</span>
                        <hr><small class="text-muted">Lorem ipsum dolor sit</small>
                      </div>
                      <div class="col-lg-7">
                        <canvas id="pieChartHome1"></canvas>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-body">
                    <div class="row align-items-center flex-row">
                      <div class="col-lg-5">
                        <h2 class="mb-0 d-flex align-items-center"><span>1.724</span><span class="dot bg-violet d-inline-block ml-3"></span></h2><span class="text-muted text-uppercase small">Server time</span>
                        <hr><small class="text-muted">Lorem ipsum dolor sit</small>
                      </div>
                      <div class="col-lg-7">
                        <canvas id="pieChartHome2"></canvas>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </section>

        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>
