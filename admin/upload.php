<?php 
session_start();
include('../function/config.php');
include('../function/helper.php');


$id = $_SESSION['profile_id'];

if (!empty($_FILES['file'])) {
	$files = $_FILES['file'];

	$file_location = '../webroot/uploads/'.$id;


	$file_name = $files['name'];
	$file_tmp = $files['tmp_name'];
	$file_type = $files['type'];
	$file_size = $files['size'];
	$file_error = $files['error'];

	$file_ext = explode('.', $file_name);
	$extention = strtolower(end($file_ext));


	if ($file_error === 0) {
		if ($file_size < 1000000) {
			if (!file_exists($file_location)) {
				mkdir($file_location);
			}

			$new_filename = uniqid('', true).'.'.$extention;
			$destination = $file_location.'/'.$new_filename;
			move_uploaded_file($file_tmp, $destination);

			?>
			<div class="animated jackInTheBox" id="previews">
				<input type="hidden" name="fie_path" id="file_path" value="<?php echo $new_filename ?>">

				<?php if (strtoupper($extention) == 'png' || strtoupper($extention) == 'jpg' || strtoupper($extention) == 'jpeg' || strtoupper($extention) == 'PNG' || strtoupper($extention) == 'JPG' || strtoupper($extention) == 'JPEG'): ?>
						<img src="<?php echo $destination ?>" class="img-fluid rounded  " width="110">
					<?php else: ?>
						<img src="../webroot/img/files.png" class="img-fluid rounded  " width="110">
						<div class="text-sm"><?php echo $file_name ?></div>
				<?php endif ?>
				
				
                <button class="btn btn-danger btn-block mt-1" id="btn_delete" onclick="delete_files('<?php echo $destination ?>');">Delete</button>
			</div>
			<?php
			
		}else{
			echo 0;
		}
	}

	



}

 ?>