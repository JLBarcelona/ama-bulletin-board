<?php 
  $able = ($auth['user_type'] == 1)? '' : 'none';
 ?>


 <div id="sidebar" class="sidebar py-3" style="margin-top: 4.3%; position: fixed;top: 0;right: 0;left: 0;z-index: 100; height:100%;">
    <div class="text-center py-1">
       <img src="../webroot/admin/img/<?php echo $auth['gender'] ?>.png" alt="Jason Doe" style="max-width: 4.5rem;" class="img-fluid rounded-circle shadow">
      <strong class="d-block text-uppercase headings-font-family"><?php echo $auth['fn'].' '.$auth['ln'] ?></strong><small><?php echo $auth['position'] ?></small>
    </div>


    <ul class="sidebar-menu list-unstyled">
          <li class="sidebar-list-item"><a href="index.php" class="sidebar-link text-muted" id="home"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>


          <li class="sidebar-list-item"><a href="posting.php" id="posting" class="sidebar-link text-muted"><i class="fa fa-chalkboard mr-3 text-gray"></i><span>Post</span></a></li>


          <li class="sidebar-list-item"><a href="dean.php" id="deans" class="sidebar-link text-muted"><i class="fa fa-list mr-3 text-gray"></i><span>Dean's Lister</span></a></li>


         
    </ul>

    <div class="text-gray-400 text-uppercase px-3 px-lg-4 font-weight-bold small headings-font-family" style="display: <?php echo $able ?>">User Management</div>
    <ul class="sidebar-menu list-unstyled">
          <li class="sidebar-list-item" style="display: <?php echo $able ?>"><a href="user.php" id="users" class="sidebar-link text-muted"><i class="o-user-details-1 mr-3 text-gray"></i><span>User Account</span></a></li>
    </ul>

    <div class="text-gray-400 text-uppercase px-3 px-lg-4  font-weight-bold small headings-font-family">My Account</div>
    <!-- <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">EXTRAS</div> -->
    <ul class="sidebar-menu list-unstyled">
          
          <li class="sidebar-list-item"><a href="edit_profile.php" id="myprofile" class="sidebar-link text-muted"><i class="o-user-1 mr-3 text-gray"></i><span>Edit Profile</span></a></li>

           <li class="sidebar-list-item"><a href="change_password.php" id="mypassword" class="sidebar-link text-muted"><i class="o-settings-window-1 mr-3 text-gray"></i><span>Change Password</span></a></li>

          <!-- li class="sidebar-list-item"><a href="logout.php" class="sidebar-link text-muted"><i class="o-exit-1 mr-3 text-gray"></i><span>Logout</span></a></li> -->
    </ul>

    <div class="text-gray-400 text-uppercase px-3 px-lg-4  font-weight-bold small headings-font-family">System Management</div>
    <!-- <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">EXTRAS</div> -->
    <ul class="sidebar-menu list-unstyled">
          <li class="sidebar-list-item" style="display: <?php echo $able ?>"><a href="recovery.php" id="recovery" class="sidebar-link text-muted"><i class="fa fa-recycle mr-3 text-gray"></i><span>Post Recovery</span></a></li>

          <li class="sidebar-list-item" style="display: <?php echo $able ?>"><a href="db_backup.php" id="db" class="sidebar-link text-muted"><i class="o-data-storage-1 mr-3 text-gray"></i><span>Database Settings</span></a></li>
    </ul>
  </div>

  <script type="text/javascript">
    
  function sidebar_active(id){
    $("#"+id).addClass('active');
  }
  </script>