<!DOCTYPE html>
<html>
    
    <?php include("header.php") ?>
  <body onload="show_wall(); set_show_others_wall_update();  sidebar_active('mypassword');  show_others_wall();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">Change Password</h2>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                          <input type="password" id="old_password" class="form-control" placeholder="Old Password">
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                          <input type="password" id="new_password" class="form-control" placeholder="New Password">
                        </div>
                      </div>

                      <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                          <input type="password" id="confirm_password" class="form-control" placeholder="Re-enter Password">
                        </div>
                      </div>
                    </div>


                    <div class="form-group text-right">
                      <button class="btn btn-primary" onclick="change_password();">Save Changes</button>
                    </div>
                   
                  </div>
                </div>

              
              </div>


         

            </div>
          </section>

        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>
<script type="text/javascript">
  function change_password(){
    var old_password = $("#old_password");
    var new_password = $("#new_password");
    var confirm_password = $("#confirm_password");


    if (old_password.val() == "") {
      old_password.focus();
    }
    else if (new_password.val() == "") {
      new_password.focus();
    }
    else if (confirm_password.val() == "") {
      confirm_password.focus();
    }else if (new_password.val() != confirm_password.val()) {
      confirm_password.focus();
      swal("Invalid","Password doesn\'t match!","error");
    }else{
      var mydata = "action=change_password_account" + '&old_password=' + old_password.val() +'&confirm_password=' + confirm_password.val();

      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             swal("Success","Password has been change!","success");
             old_password.val('');
             new_password.val('');
             confirm_password.val('');

          }else if (data.trim() == 2) {
             old_password.focus();
             swal("Invalid","Password is incorrect!","error");
          }
        }
      });
    }
  }
</script>