  <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 shadow fixed-top" style="background-color: #bd0f0f;">


        <a href="#" class="sidebar-toggler text-white mr-4 mr-lg-5 lead"><img src="../webroot/img/logo.png" width="50" class="img-fluid"></a>
        <a href="index.html" class="navbar-brand font-weight-bold text-uppercase text-white">AMA Bulletin Board</a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item dropdown ml-auto">
            <a id="userInfo" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="../webroot/admin/img/<?php echo $auth['gender'] ?>.png" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>

            <div aria-labelledby="userInfo" class="dropdown-menu">
              <a href="#" class="dropdown-item">
                <strong class="d-block text-uppercase headings-font-family"><?php echo $auth['fn'].' '.$auth['ln'] ?></strong>
                <center><small><?php echo $auth['position'] ?></small></center>
             </a>

             <a href="logout.php" class="dropdown-item">
                <strong class="d-block text-uppercase headings-font-family"><i class="fa fa-sign-out-alt"></i>Logout</strong>
             </a>

            </div>
          </li>
        </ul>
      </nav>
    </header>