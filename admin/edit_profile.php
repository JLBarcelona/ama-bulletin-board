<!DOCTYPE html>
<html>
    
    <?php include("header.php") ?>
  <body onload=" sidebar_active('myprofile'); on_edit=1; on_profile = 1;  edit('<?php echo $auth['profile_id'] ?>','<?php echo $auth['fn'] ?>','<?php echo $auth['mn'] ?>','<?php echo $auth['ln'] ?>','<?php echo $auth['gender'] ?>','<?php echo $auth['email_address'] ?>','','<?php echo $auth['department'] ?>','<?php echo $auth['position'] ?>');">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-7 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">Edit Profile</h2>
                  </div>
                  <div class="card-body">
                     <div class="row">
                        <div class="col-sm-6 form-group">
                          <input type="hidden" name="profid" id="profid" value="<?php echo $auth['profile_id'] ?>">
                          <label>Firstname</label>
                          <input type="text" name="fn" id="fn" class="form-control" placeholder="Firstname" autocomplete="off">
                        </div>
                        <div class="col-sm-6 form-group">
                          <label>Middlename</label>
                          <input type="text" name="mn" id="mn" class="form-control" placeholder="Middlename" autocomplete="off">
                        </div>
                        <div class="col-sm-6 form-group">
                          <label>Lastname</label>
                          <input type="text" name="ln" id="ln" class="form-control" placeholder="Lastname" autocomplete="off">
                        </div>
                        <div class="col-sm-6 form-group">
                          <label>Gender</label>
                          <select name="gender" id="gender" class="form-control">
                            <option selected="" disabled="">Select Gender</option>
                            <option>Male</option>
                            <option>Female</option>
                          </select>
                        </div>

                         <div class="col-sm-6 form-group" style="display: none;">
                            <label>Department</label>
                            <select name="department" id="department" class="form-control">
                              <option selected=""  value="<?php echo $auth['department'] ?>"><?php echo $auth['department'] ?></option>
                            </select>
                          </div>

                          <div class="col-sm-6 form-group" style="display:none ;">
                            <label>Position</label>
                            <select name="position" id="position" class="form-control">
                              <option selected="" value="<?php echo $auth['position'] ?>"><?php echo $auth['position'] ?></option>
                             
                            </select>
                          </div>

                        <!-- Account -->
                        <div class="col-sm-12 form-group">
                          <label>E-mail</label>
                          <input type="text" name="email" id="email" class="form-control" placeholder="E-mail address" autocomplete="off">
                        </div>

                         <div class="col-sm-12 form-group">
                          <!-- <label>Password</label> -->
                          <input type="hidden" name="password" id="password" class="form-control" placeholder="Password" autocomplete="off">
                        
                          <input type="hidden" name="sec_question" id="sec_question">
                          <input type="hidden" name="sec_answer" id="sec_answer">
                        </div>
                      </div>


                    <div class="form-group text-right">
                      <button class="btn btn-primary" onclick="on_edit = 1; save_account();">Save Changes</button>
                    </div>
                   
                  </div>
                </div>

              
              </div>


         

            </div>
          </section>

        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>
