<!DOCTYPE html>
<html>
    
  <?php include("header.php") ?>
  <body onload="sidebar_active('db'); show_database_backup();">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-sm-4">
                        <h2 class="h6 text-uppercase mb-0">Database Backup</h2>
                      </div>
                      <div class="col-sm-8 text-right">
                        <button class=" btn btn-success mr-1" onclick="backup_db();"><i class="fa fa-database"></i> Backup</button>
                        <!-- <button class=" btn btn-primary">Restore</button> -->
                      </div>
                    </div>
                  </div>
                  <div class="card-body">
                    <table class="table table-bordered" id="tbl_database">
                      <thead>
                        <tr>
                          <th>File Name</th>
                          <th width="5%" class="text-center" >Option</th>
                        </tr>
                      </thead>
                      <tbody id="data_records">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
    
      </div>
    </div>
    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>

<script type="text/javascript">
  function show_database_backup(){
    $.ajax({
      type:"POST",
      url:url,
      data:"action=database_files",
      cache:false,
      success:function(data){
        $("#data_records").html(data);
        $("#tbl_database").dataTable();

      }
    })
  }



 

  function backup_db(){
    swal({
      title: "Are you sure?",
      text: "Do you want to create backup database ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
       $.ajax({
          type:"POST",
          url:url,
          data:"action=backup_db",
          cache:false,
          success:function(data){
            console.log(data.trim());
            if (data.trim() == 1) {
              show_database_backup();
              swal("Success","Database backup file has been successfuly created!","success");
            }else{
              console.log(data,trim());
            }
          }
        });
    });
   
  }

  function execute_backup(dbname,filepath){
      swal({
        title: "Dangerous!",
        text: "Do you want to execute "+dbname+"? \n\n Current record will be permanently deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(){
         $.ajax({
            type:"POST",
            url:url,
            data:"action=execute_db" + '&file_path=' + filepath,
            cache:false,
            beforeSend:function(){
                // swal({title: "Loading...",
                //     text: "Please Wait! \n Do not turn off your computer!\n\n",
                //     showCancelButton: false,
                //     showConfirmButton: false
                //   });             
            },
            success:function(data){
              console.log(data.trim());
              if (data.trim() == 0) {
                show_database_backup();
                swal("Success","Database imported successfuly !","success");
              }else{
                swal("Success","You have "+data.trim()+" error!","error");
                console.log(data,trim());
              }
            }
          });
      });
  }
</script>
