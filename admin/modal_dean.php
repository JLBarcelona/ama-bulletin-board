 <div class="modal fade" role="dialog" id="modal_posting">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
              <h2 class="h6 text-uppercase mb-0">Deans Lister</h2>
            </div>
              <button class="close" data-dismiss="modal" onclick="cancel_edit_post();">&times;</button>
          </div>
          <div class="modal-body">
              <input type="hidden" name="post_id" id="post_id">        
               <div class="form-group" id="is_file">
                 <div class=" text-uppercase font-weight-bold small headings-font-family mb-2">Upload File</div>
                  <form id="upload" method="post" action="dean_upload.php" enctype="multipart/form-data">
                    <div id="drop">
                      Drop Here
                      <br>
                      <a>Browse</a>
                      <input type="file" name="upl" multiple />
                    </div>

                    <ul>
                      <!-- The file uploads will be shown here -->
                    </ul>

                  </form>


                   <div class="row">
                     <div class="col-sm-3">
                         <!-- <div id="img_preview"> -->
                            <textarea hidden="" name="fie_path" id="file_path"></textarea>
                          <!-- </div> -->
                     </div>
                </div>
              </div>

              </div>
              <div class="form-group text-right">
                <button class="btn btn-secondary" style="display: none;" id="btn_cancel" onclick="cancel_edit_post_dean();">Cancel</button>
                <!-- <button class="btn btn-dark" onclick="show_array();"></button> -->
                <button class="btn btn-primary" onclick="post_wall_dean();">Post</button>
              </div>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      var file_input_val = [];

    

      function show_array(){
        // console.log(file_input_val);
        var new_data = file_input_val.join("~");
        // console.log(new_data);
        $("#file_path").val(new_data);
      }

      function show_all_input_value(array,key){
        var new_data = '';

        for (var i = 0; i < array.length ; i++) {
           if (key === array[i]) {
            array.splice(i, 1);
            // console.log(array[i]);
            // console.log(key);
           }
        }

        // console.log(array);
  
      }





    function post_wall_dean(){
    var file_path = $("#file_path");
    var post_id = $("#post_id");
    // alert($("#file_name").val());
  
    if (file_path.val() == "" || file_path.val() == null) {
      // exp_date.focus();
      swal("Oops!","Please upload deans lister!","error");
    }
    else{
      var mydata = 'action=save_announcement_dean' + '&file_path=' + file_path.val() + '&post_id=' + post_id.val();
      
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          $("#loader").html('<center><img src="../webroot/img/load.gif" class="img-fluid mt-4"  width="50"></center>');
        },
        success:function(data){
        
          if (data.trim() == 1) {
            swal("Success","Dean has been updated!","success");
            location.reload();
          }else{
            alert(data);
          }
        }
      });

    }


  }


//   function delete_post_new(id, name, type){
//     var message = '';

//     if (type == 1) {
//       message ='delete';
//     }else if (type == 0) {
//       message= 'acivate';
//     }

//   swal({
//       title: "Are you sure?",
//       text: "Do you want to "+message+" "+name+"?",
//       type: "warning",
//       showCancelButton: true,
//       confirmButtonColor: "#DD6B55",
//       confirmButtonText: "Yes",
//       closeOnConfirm: false
//     },
//     function(){
//       $.ajax({
//         type:"POST",
//         url:url,
//         data:'action=delete_post_new' + '&id=' + id + '&type=' +type,
//         cache:false,
//         success:function(data){
//           if (data.trim() == 1) {
//              show_all_post();
//              swal("Success","Post has been "+message+"d!","success");
//           }else{
//             console.log(data);
//           }
//         }
//       });
//     });
// }
    </script>