<!DOCTYPE html>
<html>
    
    <?php include("header.php") ?>
  <body onload="show_accounts();  sidebar_active('users');">
    <!-- navbar-->
    <?php include("nav.php") ?>
    
    <div class="d-flex align-items-stretch">
    <?php include("sidebar.php") ?>
     
      <div class="page-holder w-100 d-flex flex-wrap mt-5" style="margin-left:20%;">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">
                      <i class="fa fa-user-circle"></i> User Account
                     <button class="btn btn-success float-right" data-toggle="modal" data-backdrop="static" data-target="#add_account"><i class="fa fa-plus-circle"></i> Add Account</button>
                    </h2>
                  </div>
                  <div class="card-body">
                    <table class="table card-text" id="tbl_accounts">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Gender</th>
                          <th>E-mail</th>
                          <th class="text-center">Status</th>
                          <th class="text-center">Option</th>
                        </tr>
                      </thead>
                      <tbody id="account_data">

                      </tbody>
                    </table>
                   
                  </div>
                </div>

              
              </div>
            </div>
          </section>
        </div>
    
      </div>
    </div>


    <!-- Modal -->

    <div class="modal fade" role="dialog" id="add_account">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title"><i class="fa fa-user-plus"></i> Register Account</div>
            <button class="close" type="button" data-dismiss="modal" onclick="edit('','','','','','','');">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-6 form-group">
                <input type="hidden" name="profid" id="profid">
                <label>Firstname</label>
                <input type="text" name="fn" id="fn" class="form-control" placeholder="Firstname" autocomplete="off">
              </div>
              <div class="col-sm-6 form-group">
                <label>Middlename</label>
                <input type="text" name="mn" id="mn" class="form-control" placeholder="Middlename" autocomplete="off">
              </div>
              <div class="col-sm-6 form-group">
                <label>Lastname</label>
                <input type="text" name="ln" id="ln" class="form-control" placeholder="Lastname" autocomplete="off">
              </div>

              <div class="col-sm-6 form-group">
                <label>Sex</label>
                <select name="gender" id="gender" class="form-control">
                  <option selected="" disabled="">Select Gender</option>
                  <option>Male</option>
                  <option>Female</option>
                </select>
              </div>
              

              <div class="col-sm-6 form-group">
                <label>Department</label>
                <select name="department" id="department" class="form-control">
                  <option selected="" disabled="">Select Department</option>
                  <option>Senior High</option>
                  <option>College</option>
                </select>
              </div>

              <div class="col-sm-6 form-group">
                <label>Position</label>
                <select name="position" id="position" class="form-control">
                  <option selected="" disabled="">Select Position</option>
                  <option>Area Coordinator</option>
                  <option>Assistant Instructor</option>
                  <option>SHS Coordinator</option>
                </select>
              </div>

              <!-- Account -->
              <div class="col-sm-12 form-group">
                <label>username</label>
                <input type="text" name="username" id="email" class="form-control" placeholder="Username" autocomplete="off">
              </div>

               <div class="col-sm-12 form-group">
                <label>Password</label>
                <form>
                  <input type="password" name="password" id="password" class="form-control" placeholder="Password" autocomplete="off">
                </form>
                
              </div>

              <div class="col-sm-12">
                <div class="row p-0">
                  <div class="col-sm-3"><hr></div>
                  <div class="col-sm-6 text-danger text-center">For security purpose only!</div>
                  <div class="col-sm-3"><hr></div>
                </div>
              </div>
              <div class="col-sm-12 form-group">
                <label>Security Question</label>
                <select class="form-control" id="sec_question">
                  <option></option>
                  <option>What was your childhood nickname?</option>
                  <option>In what city did you meet your spouse/significant other?</option>
                  <option>What is the name of your favorite childhood friend?</option>
                  <option>What street did you live on in third grade?</option>
                  <option>What school did you attend for sixth grade?</option>
                </select>
              </div>
              <div class="col-sm-12 form-group">
                <label>Answer</label>
                <input type="text" id="sec_answer" class="form-control">
              </div>
            
            </div>
          </div>
          <div class="modal-footer text-right">
            <button class="btn btn-primary" onclick="save_account();"><i class="fa fa-save"></i> Save</button>
          </div>
        </div>
      </div>
    </div>

    <!-- JavaScript files-->
    <?php include("footer.php") ?>

  </body>
</html>

<script type="text/javascript">

</script>
