<?php 
session_start();
if (!isset($_SESSION['profile_id'])) {
    @header('location:../');
}

include('../function/config.php'); 
include('../function/helper.php'); 
$auth = get_session($con,$id);
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AMA Bulletin Board Login </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="../webroot/admin/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="../assets/font/css/all.css">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href="../webroot/admin/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="../webroot/admin/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="../webroot/admin/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../webroot/img/logo.png">
    
    <link href="../webroot/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
   
    <link rel="stylesheet" href="../assets/css/sweetalert.css">
    <link rel="stylesheet" href="../assets/css/animate.css">
    <link rel="stylesheet" href="../webroot/upload_plugin/css/style.css">
    <!-- <link rel="stylesheet" href="../assets/css/themes/twitter/twitter.css"> -->
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <style type="text/css">
      .dropdown.no-arrow .dropdown-toggle::after {
          display: none;
        }
  </style>