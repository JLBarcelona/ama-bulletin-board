<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AMA Bulletin Board</title>
   <link rel="shortcut icon" href="webroot/img/logo.png">
  <!-- Font Awesome Icons -->
  <link href="webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="webroot/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="webroot/css/creative.min.css" rel="stylesheet">
  <link href="assets/css/animate.css" rel="stylesheet">

  <style type="text/css">
    /* width */
    ::-webkit-scrollbar {
      width: 8px;
      border-radius:5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
      border-radius:5px;
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
      border-radius:5px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    }
    .post-wall{
      overflow: auto;
      overflow-x: hidden;
      height: 500px;
      max-height: 500px;
      font-family: calibri;
    }
  </style>
</head>

<body id="page-top" onload=" show_all_trigger(); show_all_announcement();" style="background-color: #f1f1f1; overflow-y: hidden;">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg fixed-bottom py-3" id="mainNav" style="background-color: transparent !important; box-shadow: 0 0.5rem 1rem rgba(0,0,0,.0)">
    <div class="container-fluid">
   <!--    <div id="txt" class="h1 float-right rounded bg-info text-white p-2 shadow" style="font-family: calibri; background-color: #f44336!important;">00:00</div> -->
    </div>
  </nav>

  <!-- Masthead -->
  
   <!--  <div class="container-fluid ">
      <div class="row ">
       
      <div class="col-sm-12 p-0"> 
            <div id="demo" class="carousel slide " data-ride="carousel" style="padding: 0px;">

            <ul class="carousel-indicators">
              <li data-target="#demo" data-slide-to="0" class="active"></li>
              <li data-target="#demo" data-slide-to="1"></li>
              <li data-target="#demo" data-slide-to="2"></li>
            </ul>


            <div class="carousel-inner" style="padding: 0px;">

              <div class="carousel-item active">
                <img src="webroot/img/portfolio/fullsize/1.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
              </div>
              <div class="carousel-item">
               <img src="webroot/img/portfolio/fullsize/2.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
              </div>
              <div class="carousel-item">
                <img src="webroot/img/portfolio/fullsize/3.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
              </div>
            </div>

            <a class="carousel-control-prev " href="#demo" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next " href="#demo" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>

          </div>

        <section class="page-section p-3" id="about" style="background-color: #c3283a!important;">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-5 text-center">
              <h4 class="text-white mt-0">AMA UNIVERSITY AND COLLEGES</h4>
              <hr class="divider light my-4">
              <p class="text-white mb-4 ">AMA University is a member of the AMA Education System and the first in IT education and full online education in the Philippines.</p>
            </div>
            <div class="col-lg-2"></div>
             <div class="col-lg-5 text-center">
              <h4 class="text-white mt-0">VISION & MISSION</h4>
              <hr class="divider light my-4">
              <p class="text-white mb-4 ">AMA will be the leader and dominant provider of relevant globally recognized information technology-based education and related services in the global market…</p>
            </div>
          </div>
        </div>
      </section>
      </div>
      <div class=""> </div>
      
   
        </div> -->

       <!--  <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Your Favorite Source of Free Bootstrap Themes</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">Start Bootstrap can help you build better websites using the Bootstrap framework! Just download a theme and start customizing, no strings attached!</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
        </div> -->
      </div>
    </div>

  <!-- About Section -->

 

  <div class="p-0" id="services" style="padding-left:10px;  max-height:300 !important; height: 300; width: 100% !important; max-width:100% !important; ">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
           <marquee scrolldelay="200"><b>
             (AMA UNIVERSITY AND COLLEGES)</b> -AMA University is a member of the AMA Education System and the first in IT education and full online education in the Philippines. •

             <b>(VISION & MISSION)</b>  -AMA will be the leader and dominant provider of relevant globally recognized information technology-based education and related services in the global market •
           </marquee>
        </div>
        <div class="col-lg-4">
          <div class="mt-0">
            <div class="text-white p-3 rounded" style="background-color: #bb1919;">
                <img src="webroot/img/megaphone.png" class="img-fluid mb-3"  width=90>
                 <span class="h4 mb-2">Announcement</span>
            </div>
         
            <hr>
           <div class="p-1 post-wall">
            
          <div id="announcement_board"></div>
            
            </div>
            
          </div>
        </div>


          <div class="col-lg-4">
           <div class="mt-0">
             <div class="text-white p-3 rounded" style="background-color: #bb1919;">
               <img src="webroot/img/sched.png" class="img-fluid mb-3"  width=90>
                <span class="h4 mb-2">Schedule</span>
              </div>
              <hr>
              <div class="p-1 post-wall" >
                 <div id="schedule_board"></div>
                </div>
            </div>
          </div>

          <div class="col-lg-4">
           <div class="mt-0">
             <div class="text-white p-3 rounded" style="background-color: #bb1919;">
               <img src="webroot/img/act.png" class="img-fluid mb-3"  width=90>
                <span class="h4 mb-2">Activity</span>
              </div>
              <hr>
              <div class="p-1 post-wall">
                 <div id="activity_board"></div>
                </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript -->
  <script src="webroot/vendor/jquery/jquery.min.js"></script>
  <script src="webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="webroot/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="webroot/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="webroot/js/creative.min.js"></script>

</body>

</html>

<script>

var url = "function/function.php";


function show_all_board(elem,type){
$.ajax({
  type:"POST",
  url:url,
  data:'action=show_index' + '&type=' + type,
  cache:false,
  success:function(data){
    $("#"+elem).html(data);
  }
});
}

function show_all_announcement(){
  show_all_board('announcement_board','Announcement');
  show_all_board('schedule_board','Schedule');
  show_all_board('activity_board','Activity');
}


function show_all_trigger(){
 setInterval(function(){
   show_all_announcement();
  },4000);
}




function startTime() {
  var today = new Date();

  var s = today.getSeconds();
  var h = today.getHours();
  var m = today.getMinutes();
  var ampm = h >= 12 ? 'pm' : 'am';
  h = h % 12;
  h = h ? h : 12; // the hour '0' should be '12'
  m = m < 10 ? '0'+m : m;
  var strTime = h + ':' + m + ' ' + ampm;
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('txt').innerHTML =
  strTime;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>