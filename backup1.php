<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>AMA Bulletin Board</title>
   <link rel="shortcut icon" href="webroot/img/logo.png">
  <!-- Font Awesome Icons -->
  <link href="webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

  <!-- Plugin CSS -->
  <link href="webroot/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="webroot/css/creative.min.css" rel="stylesheet">
  <link href="assets/css/animate.css" rel="stylesheet">

  <style type="text/css">
    /* width */
    ::-webkit-scrollbar {
      width: 8px;
      border-radius:5px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: #f1f1f1; 
      border-radius:5px;
    }
     
    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #888; 
      border-radius:5px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #555; 
    }
    .post-wall{
      overflow: auto;
      overflow-x: hidden;
      height: 500px;
      max-height: 500px;
      font-family: calibri;
    }
    #myVideo {
    position: fixed;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;

   
}

/* Add some content at the bottom of the video/page */
.content {
  position: fixed;
  bottom: 0;
  background: rgba(0, 0, 0, 0.5);
  color: #f1f1f1;
  width: 100%;
  padding: 20px;
}

/* Style the button used to pause/play the video */
#myBtn {
  width: 100px;
  font-size: 18px;
  background: #000;
  color: #fff;
  cursor: pointer;
  opacity: 0.1;
  transition: opacity 0.5s;
}

#myBtn:hover {
  /*background: #ddd;*/
  /*color: black;*/
  opacity: 1;

}
  </style>
</head>


<script type="text/javascript">
  function flip_board(){
    $("#btnicon").toggleClass('fa-bars fa-times');
  }
</script>
<body id="page-top" onload=" show_all_trigger(); show_all_announcement(); show_all_marquee();" style="background-color: #f1f1f1; overflow: hidden;">

  <!-- Navigation -->
  <nav class="navbar navbar-expand navbar-dark navbar-light fixed-top" style="background: rgba(0,0,0,0.2);">
    <div class="container-fluid">
      <button class="btn btn-dark" onclick="flip_board();"><i id="btnicon" class="fa fa-bars"></i></button>
    </div>
  </nav>

  <!-- Masthead -->
  
    <div class="container-fluid ">
      <div class="row text-center">

        <video autoplay muted loop id="myVideo">
          <source src="ama.mp4" type="video/mp4">
        </video>
       
       <div class="content">
        <marquee>
          <div id="announcement_panel">
          </div>
        </marquee>
        <!-- Use a button to pause/play the video with JavaScript -->
        <button id="myBtn" class="btn btn-dark btn-sm" onclick="myFunction()">Pause</button>
      </div>
        <!-- <div id="demo" class="carousel slide col-sm-12" data-ride="carousel" style="padding: 0px;"> -->

          <!-- Indicators -->
       <!--    <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul> -->


          <!-- The slideshow -->
          <!-- <div class="carousel-inner" style="padding: 0px;">

            <div class="carousel-item active">
              <img src="webroot/img/portfolio/fullsize/1.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
            </div>
            <div class="carousel-item">
             <img src="webroot/img/portfolio/fullsize/2.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
            </div>
            <div class="carousel-item">
              <img src="webroot/img/portfolio/fullsize/3.jpg" alt="Los Angeles" class="animated fadeIn" style="width: 100%;">
            </div>
          </div> -->

          <!-- Left and right controls -->
          <!-- <a class="carousel-control-prev " href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next " href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>

        </div>
   
        </div> -->

       <!--  <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">Your Favorite Source of Free Bootstrap Themes</h1>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">Start Bootstrap can help you build better websites using the Bootstrap framework! Just download a theme and start customizing, no strings attached!</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
        </div> -->
      <!-- </div> -->
    </div>

  <!-- About Section -->

 <!-- <section class="page-section" id="about" style="background-color: #c3283a!important;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-5 text-center">
          <h4 class="text-white mt-0">AMA UNIVERSITY AND COLLEGES</h4>
          <hr class="divider light my-4">
          <p class="text-white mb-4 ">AMA University is a member of the AMA Education System and the first in IT education and full online education in the Philippines.</p>
        </div>
        <div class="col-lg-2"></div>
         <div class="col-lg-5 text-center">
          <h4 class="text-white mt-0">VISION & MISSION</h4>
          <hr class="divider light my-4">
          <p class="text-white mb-4 ">AMA will be the leader and dominant provider of relevant globally recognized information technology-based education and related services in the global market…</p>
        </div>
      </div>
    </div>
  </section> -->

  <div class="page-section" id="services" style="display: none; padding-left:10px; padding-top: 10px; max-height:710px !important; height: 710px; width: 100% !important; max-width:100% !important; ">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3">
          <div class="mt-5">
            <div class="bg-white p-3 rounded">
                <img src="webroot/img/megaphone.png" class="img-fluid mb-3"  width=90>
                 <span class="h4 mb-2">Announcement</span>
            </div>
         
            <hr>
           <div class="p-1 post-wall">
            
          <div id="announcement_board"></div>
            
            </div>
            
          </div>
        </div>


          <div class="col-lg-6">
           <div class="mt-5">
             <div class="bg-white p-3 rounded">
               <img src="webroot/img/sched.png" class="img-fluid mb-3"  width=90>
                <span class="h4 mb-2">Schedule</span>
              </div>
              <hr>
              <div class="p-1 post-wall" >
                 <div id="schedule_board"></div>
                </div>
            </div>
          </div>

          <div class="col-lg-3">
           <div class="mt-5">
             <div class="bg-white p-3 rounded">
               <img src="webroot/img/act.png" class="img-fluid mb-3"  width=90>
                <span class="h4 mb-2">Activity</span>
              </div>
              <hr>
              <div class="p-1 post-wall">
                 <div id="activity_board"></div>
                </div>
            </div>
          </div>

         

        </div>
      </div>
    </div>
  </div>



  <!-- Bootstrap core JavaScript -->
  <script src="webroot/vendor/jquery/jquery.min.js"></script>
  <script src="webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="webroot/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="webroot/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="webroot/js/creative.min.js"></script>

</body>

</html>

<script>

var url = "function/function.php";

function show_all_marquee(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=show_announcement',
    cache:false,
    success:function(data){
      $("#announcement_panel").html(data);
    }
  });
}


function show_all_board(elem,type){
$.ajax({
  type:"POST",
  url:url,
  data:'action=show_index' + '&type=' + type,
  cache:false,
  success:function(data){
    $("#"+elem).html(data);
  }
});
}

function show_all_announcement(){
  show_all_board('announcement_board','Announcement');
  show_all_board('schedule_board','Schedule');
  show_all_board('activity_board','Activity');
}


function show_all_trigger(){
 setInterval(function(){
   show_all_announcement();
  },4000);
}




function startTime() {
  var today = new Date();

  var s = today.getSeconds();
  var h = today.getHours();
  var m = today.getMinutes();
  var ampm = h >= 12 ? 'pm' : 'am';
  h = h % 12;
  h = h ? h : 12; // the hour '0' should be '12'
  m = m < 10 ? '0'+m : m;
  var strTime = h + ':' + m + ' ' + ampm;
  m = checkTime(m);
  s = checkTime(s);
  document.getElementById('txt').innerHTML =
  strTime;
  var t = setTimeout(startTime, 500);
}
function checkTime(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}
</script>

<script>
// Get the video
var video = document.getElementById("myVideo");

// Get the button
var btn = document.getElementById("myBtn");

// Pause and play the video, and change the button text
function myFunction() {
  if (video.paused) {
    video.play();
    btn.innerHTML = "Pause";
  } else {
    video.pause();
    btn.innerHTML = "Play";
  }
}
</script>